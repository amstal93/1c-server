  �'      ResB             Gx     �  �{  �{  \      �>  IDS_ERR_FORMAT_STRING IDS_ERR_UNKNOWN_LOCALE IDS_ERR_BAD_SUBJECT_PARAMS IDS_RTE_CB_CREATE IDS_RTE_CB_OPEN IDS_RTE_CB_READ IDS_RTE_CB_SEEK IDS_RTE_CB_WRITE IDS_RTE_CB_CLOSE IDS_RTE_CB_REMOVE IDS_RTE_CB_LOCK IDS_RTE_CB_UNLOCK IDS_RTE_CB_LEN IDS_RTE_CB_LEN_SET IDS_RTE_CB_RENAME IDS_RTE_CB_DATA IDS_RTE_CB_REC_LEN IDS_RTE_CB_FIELD_NAME IDS_RTE_CB_FIELD_TYPE IDS_RTE_CB_ENTRY IDS_RTE_CB_INDEX IDS_RTE_CB_TAG_NAME IDS_RTE_CB_UNIQUE IDS_RTE_CB_COMMA_EXPR IDS_RTE_CB_COMPLETE IDS_RTE_CB_DATA_NAME IDS_RTE_CB_LEN_ERR IDS_RTE_CB_NOT_CONST IDS_RTE_CB_NUM_PARMS IDS_RTE_CB_OVERFLOW IDS_RTE_CB_RIGHT_MISSING IDS_RTE_CB_TYPE_SUB IDS_RTE_CB_UNREC_FUNC IDS_RTE_CB_UNREC_OPERATOR IDS_RTE_CB_UNREC_VALUE IDS_RTE_CB_UNTERMINATED IDS_RTE_CB_OPT IDS_RTE_CB_OPT_SUSPEND IDS_RTE_CB_OPT_FLUSH IDS_RTE_CB_INFO IDS_RTE_CB_MEMORY IDS_RTE_CB_PARM IDS_RTE_CB_DEMO IDS_RTE_CB_RESULT IDS_RTE_CB_LEN_LESS_PREC IDS_RTE_DB_MUST_CLOSE IDS_RTE_UNDEFINE_FIELDS IDS_RTE_TOO_LONG_FILE_PATH IDS_RTE_TOO_LONG_FILE_NAME IDS_RTE_DB_MUST_OPEN IDS_RTE_SELECT_INDEX IDS_RTE_REC_MUST_SELECT IDS_RTE_NO_MEMORY IDS_RTE_FIELDNR_OUT IDS_RTE_COPYFILE_ERROR IDS_RTE_MOVEFILE_ERROR IDS_RTE_DELETEFILES_ERROR IDS_RTE_CREATEDIRECTORY_ERROR IDS_RTE_DIRFILENAME_ERROR IDS_RTE_FINDFILES_ERROR IDS_ERR_FILEINFO IDS_RTE_CB_CURRENTINDEX IDS_RTE_CB_ILLEGALFIELDNAME IDS_RTE_CB_ILLEGALINDEXNAME IDS_RTE_GETCOMOBJECT_ERROR IDS_TYPE_PRESENTATION_FILE IDS_TYPE_PRESENTATION_FILEREF IDS_TYPE_PRESENTATION_XBASEINDEX IDS_TYPE_PRESENTATION_XBASEINDEXES IDS_TYPE_PRESENTATION_XBASEFIELD IDS_TYPE_PRESENTATION_XBASEFIELDS IDS_TYPE_PRESENTATION_XBASEKEY IDS_TYPE_PRESENTATION_XBASE IDS_TYPE_PRESENTATION_PROXYBYPASS IDS_TYPE_PRESENTATION_PUSHNOTIFICATIONSEND IDS_TYPE_PRESENTATION_PUSHNOTIFICATION IDS_SEQTEXT_ERR_OPENMODE IDS_SEQTEXT_ERR_READ IDS_SEQTEXT_ERR_WRITE IDS_ERROR_WININETNOTAVAIL IDS_MAIL_ERR_553 IDS_MAIL_ERR_452 IDS_MAIL_ERR_454 IDS_MAIL_ERR_450 IDS_MAIL_ERR_451 IDS_MAIL_ERR_552 IDS_MAIL_ERR_504 IDS_MAIL_ERR_UNKNOWN IDS_MAIL_ERR_421 IDS_MAIL_ERR_501 IDS_MAIL_ERR_550 IDS_MAIL_ERR_TIMEOUT IDS_MAIL_ERR_SEND_TIMEOUT IDS_MAIL_ERR_NO_RCPT IDS_MAIL_ERR_USER_NOT_FOUND IDS_MAIL_ERR_CONNECT IDS_MAIL_ERR_GET_HOSTNAME IDS_MAIL_ERR_TEMPORARY_PROBLEM IDS_MAIL_ERR_NES_SERVER IDS_MAIL_ERR_NONE_SMTP_ADDRESS IDS_MAIL_ERR_NONE_POP3_ADDRESS IDS_MAIL_ERR_NONE_IMAP_ADDRESS IDS_MAIL_ERR_NOT_CONNECTED_POP3 IDS_MAIL_ERR_NOT_CONNECTED_IMAP IDS_MAIL_ERR_NOT_CONNECTED_SMTP IDS_MAIL_ERR_535 IDS_MAIL_ERR_530 IDS_MAIL_ERR_AUTHFAILED IDS_MAIL_ERR_AUTHNOMETHOD IDS_MAIL_IMAP_ERR IDS_MAIL_IMAP_INTERNAL_ERR IDS_MAIL_IMAP_CONNECTION_ERR IDS_MAIL_IMAP_PARSE_ERR IDS_MAIL_IMAP_SERVER_MESSAGE IDS_MAIL_IMAP_ERR_WITH_MESSAGE IDS_MAIL_IMAP_UNSUPPORTED_SEARCH_CRITERIA IDS_MAIL_IMAP_UNSUPPORTED_SEARCH_CRITERIA_VALUE IDS_MAIL_IMAP_UNSUPPORTED_MOVE IDS_MAIL_IMAP_OAUTH2_UNSUPPORTED IDS_MAIL_IMAP_OAUTH2_MISSING_TOKEN IDS_MAIL_POP3_ERR IDS_MAIL_POP3_ERR_WITH_MESSAGE IDS_MAIL_POP3_SERVER_CONNECTION_REFUSED IDS_MAIL_SMTP_ERR IDS_MAIL_SMTP_ERR_WITH_MESSAGE IDS_MAIL_SMTP_SERVER_CONNECTION_REFUSED IDS_MAIL_SMTP_SERVER_STREAM_ERROR IDS_MAIL_ERR_PARSE_HTML IDS_MAIL_POP_SSL_CONNECTION_ERROR IDS_EXT_ZIP64_AES128_COMPRESSION_METHOD IDS_EXT_ZIP64_AES192_COMPRESSION_METHOD IDS_EXT_ZIP64_AES256_COMPRESSION_METHOD IDS_EXT_ZIP64_COPY_COMPRESSION_METHOD IDS_EXT_ZIP64_DEFLATE64_COMPRESSION_METHOD IDS_EXT_ZIP64_DEFLATE_COMPRESSION_METHOD IDS_EXT_ZIP64_BZIP2_COMPRESSION_METHOD IDS_EXT_ZIP64_DO_NOT_STORE_PATH_STORE_METHOD IDS_EXT_ZIP64_ERROR_DESCR__WRONG_PARAMETR IDS_EXT_ZIP64_ERROR_DESCR_ACCESS_TO_DELETED_OBJECT IDS_EXT_ZIP64_ERROR_DESCR_ALREADY_EXISTS IDS_EXT_ZIP64_ERROR_DESCR_ARCH_OP_WARNING IDS_EXT_ZIP64_ERROR_DESCR_ARCHIVE_IS_ALREADY_OPENED IDS_EXT_ZIP64_ERROR_DESCR_ARCHIVE_IS_NOT_OPENED IDS_EXT_ZIP64_ERROR_DESCR_BAD_CRC IDS_EXT_ZIP64_ERROR_DESCR_BAD_FILE_FORMAT IDS_EXT_ZIP64_ERROR_DESCR_DISK_FULL IDS_EXT_ZIP64_ERROR_DESCR_ERROR_CREATE_FILE IDS_EXT_ZIP64_ERROR_DESCR_ERROR_OPEN_FILE IDS_EXT_ZIP64_ERROR_DESCR_FILE_ALREADY_ADDED IDS_EXT_ZIP64_ERROR_DESCR_FILE_NOT_FOUND IDS_EXT_ZIP64_ERROR_DESCR_INVALID_EXTRACT_PATH IDS_EXT_ZIP64_ERROR_DESCR_INVALID_NAME IDS_EXT_ZIP64_ERROR_DESCR_NO_ERROR IDS_EXT_ZIP64_ERROR_DESCR_NO_MORE_ENTRIES IDS_EXT_ZIP64_ERROR_DESCR_OBJECT_INVALID_FILE_NAME IDS_EXT_ZIP64_ERROR_DESCR_PATH_NOT_FOUND IDS_EXT_ZIP64_ERROR_DESCR_UNKNOWN_ERROR IDS_EXT_ZIP64_ERROR_DESCR_UNKNOWN_LIB_ERROR IDS_EXT_ZIP64_ERROR_DESCR_UNSUPPORTED_METHOD IDS_EXT_ZIP64_ERROR_DESCR_USER_CANCEL IDS_EXT_ZIP64_ERROR_DESCR_VOLUME_NOT_FOUND IDS_EXT_ZIP64_ERROR_DESCR_WRONG_PASSWORD IDS_EXT_ZIP64_MAXIMUM_COMPRESSION_LEVEL IDS_EXT_ZIP64_MINUMUM_COMPRESSION_LEVEL IDS_EXT_ZIP64_OBJECT_INVALID_ITEM_TO_EXTRACT IDS_EXT_ZIP64_OBJECT_INVALID_OBJECT_INDEX IDS_EXT_ZIP64_OBJECT_INVALID_PARAM IDS_EXT_ZIP64_OBJECT_INVALID_PASSWORD IDS_EXT_ZIP64_OBJECT_TYPE_PRESENTATION_ZIP_FILE_ITEM IDS_EXT_ZIP64_OBJECT_TYPE_PRESENTATION_ZIP_FILE_ITEMS IDS_EXT_ZIP64_OBJECT_TYPE_PRESENTATION_ZIP_FILE_READER IDS_EXT_ZIP64_OBJECT_TYPE_PRESENTATION_ZIP_FILE_WRITER IDS_EXT_ZIP64_OPTIMAL_COMPRESSION_LEVEL IDS_EXT_ZIP64_RECURSIVE_SUB_DIR_PROCESSING_DO_NOT_PROCESS IDS_EXT_ZIP64_RECURSIVE_SUB_DIR_PROCESSING_PROCESS IDS_EXT_ZIP64_RESTORE_FILE_PATHS_DO_NOT_RESTORE IDS_EXT_ZIP64_RESTORE_FILE_PATHS_RESTORE IDS_EXT_ZIP64_STORE_FULL_PATH_STORE_METHOD IDS_EXT_ZIP64_STORE_RELATIVE_PATH_STORE_METHOD IDS_EXT_ZIP64_ZIP20_COMPRESSION_METHOD IDS_MAIL_ERR_CHARSET_NOT_SUPPORT IDS_RTE_CB_MEMONOTSUPPORTED IDS_RTE_MERGE_FILE_ERROR IDS_RTE_SPLIT_FILE_ERROR IDS_SEQTEXT_ERR_NOTOPEN IDS_RTE_TOO_LONG_FIELD IDS_ERR_EXTRACT_TEXT IDS_TYPE_TEXT_EXTRACTION IDS_COM_ONLYINWIDOWS IDS_EXT_USER_MESSAGE_USING_ALWAYS IDS_EXT_USER_MESSAGE_USING_ON_SUCCESS_COMPLETION IDS_TYPE_PRESENTATION_USER_MESSAGE_TYPE IDS_TYPE_PRESENTATION_TRANSFERABLEFILEDESCRIPTION IDS_TYPE_PRESENTATION_TRANSFEREDFILEDESCRIPTION IDS_ERR_NOTADMIN IDS_TYPE_PRESENTATION_CRYPTOTOOLSMANAGERTYPE IDS_TYPE_PRESENTATION_CRYPTOMODULEINFOTYPE IDS_TYPE_PRESENTATION_CRYPTOMANAGERTYPE IDS_TYPE_PRESENTATION_CRYPTOCERTTYPE IDS_TYPE_PRESENTATION_CRYPTOCERTSTORETYPE IDS_GENERAL_CONTEXT_DO_CREATE_PROCESS IDS_ERR_GENERAL_CONTEXT_CMD_LINE_TOO_LONG IDS_ERR_GENERAL_CONTEXT_CANT_FIND_COMAND_INTERPRITER IDS_ERR_GENERAL_CONTEXT_COMAND_INTERPRITER_INVALID_FORMAT IDS_ERR_GENERAL_CONTEXT_NOT_ENOUGH_MEMORY IDS_ERR_GENERAL_CONTEXT_UNKNOWN_ERROR IDS_EXT_ERR_CRYPTACQUIRECONTEXTFAILED IDS_EXT_ERR_CRYPTACQUIRECONTEXTFAILED2 IDS_EXT_ERR_CRYPTGETPROVPARAMFAILED IDS_EXT_ERR_CRYPTCERTCREATECONTEXTFAILED IDS_EXT_ERR_CRYPTCERTSIZELIMIT IDS_EXT_ERR_CERTOPENSTOREFAILED IDS_EXT_ERR_CERTOPENSTOREFAILED2 IDS_EXT_ERR_DATALENGTHTOOMUCH IDS_EXT_ERR_NOPERSONALCERTIFICATES IDS_EXT_ERR_CERTIFICATENOTSELECTED IDS_EXT_ERR_CRYPTCERTCREATECERTCHAINENGFAILED IDS_EXT_ERR_CRYPTCERTGETCERTCHAINFAILED IDS_EXT_ERR_CRYPTVERIFYCHAINFAILED IDS_EXT_ERR_CERTOIDTOALGID IDS_EXT_ERR_VERIFYCERTFAIL_UNKNOWNERROR IDS_EXT_ERR_GENERALCRYPTOERROR IDS_EXT_ERR_CRYPTGETINFOFAILED IDS_EXT_ERR_CRYPTOIMPORTCERTFAILED IDS_EXT_ERR_CRYPTDELETINGCERTNOTFOUND IDS_EXT_ERR_CRYPTDELETECERTFAILED IDS_EXT_ERR_CERTGETCERTSFAILED IDS_EXT_ERR_CERTSTOREENUMFAILED IDS_EXT_ERR_CRYPTOSIGNFAILED IDS_EXT_ERR_CRYPTOVERIFYSIGNATUREFAILED IDS_EXT_ERR_CRYPTENCRYPTFAILED IDS_EXT_ERR_CRYPTDECRYPTFAILED IDS_EXT_ERR_CRYPTMSGOPENTODECODEFAILED IDS_EXT_ERR_CRYPTMSGUPDATEFAILED_READ IDS_EXT_ERR_CERTNOTFOUNDINPERSONALSTORE IDS_EXT_ERR_GETCERTCONTEXTPROPFAILED IDS_EXT_ERR_CRYPTMSGUPDATEFAILED IDS_EXT_ERR_SETPROVPARAMFAILED IDS_EXT_ERR_CRYPTMSGGETPARAMFAILED_SIGN IDS_EXT_ERR_CRYPTMSGUPDATEFAILED_DECRYPT IDS_EXT_ERR_CRYPTMSGGETPARAMFAILED IDS_EXT_ERR_CRYPTMSGCONTROLFAILED IDS_EXT_ERR_PERSONALCERTNOTFOUND IDS_EXT_ERR_CERTNOTLINKEDWITHPRIVATEKEY IDS_EXT_ERR_CERTSTOREREADONLY IDS_EXT_ERR_CSPDOESNTSUPPORTPRIVATEKEYPASSWORDSETTING IDS_EXT_ERR_MODULENOTLOADED IDS_EXT_ERR_INVALIDDATE IDS_EXT_ERR_MBSTOWCS IDS_EXT_ERR_WCSTOMBS IDS_PROPVAL_CALENDAR_SINGLEDATE_SELECTION IDS_PROPVAL_CALENDAR_MULTIDATE_SELECTION IDS_PROPVAL_CALENDAR_PERIOD_SELECTION IDS_TODAY IDS_EXT_ERR_PLURALS_TYPE_MISSED IDS_EXT_ERR_PLURALS_INCOMPLETE_RULE IDS_EXT_ERR_PLURALS_UNEXPECTED_SYMBOL IDS_EXT_ERR_PLURALS_INCORRECT_EXP IDS_EXT_ERR_PLURALS_UNKNOWN_CATEGORY IDS_EXT_ERR_PLURALS_NOT_A_NUMBER IDS_EXT_ERR_PLURALS_MISSED_CARDINAL_SET IDS_EXT_ERR_PLURALS_LOCALE_MISSED IDS_EXT_ERR_MATCH_WITH_NUMBER_INCORRECT_CATEGORIES_NUMBER IDS_EXT_ERR_CERT_E_EXPIRED IDS_EXT_ERR_CERT_E_VALIDITYPERIODNESTING IDS_EXT_ERR_CERT_E_ROLE IDS_EXT_ERR_CERT_E_PATHLENCONST IDS_EXT_ERR_CERT_E_CRITICAL IDS_EXT_ERR_CERT_E_PURPOSE IDS_EXT_ERR_CERT_E_ISSUERCHAINING IDS_EXT_ERR_CERT_E_MALFORMED IDS_EXT_ERR_CERT_E_UNTRUSTEDROOT IDS_EXT_ERR_CERT_E_CHAINING IDS_EXT_ERR_CERT_E_REVOKED IDS_EXT_ERR_CERT_E_UNTRUSTEDTESTROOT IDS_EXT_ERR_CERT_E_REVOCATION_FAILURE IDS_EXT_ERR_CERT_E_CN_NO_MATCH IDS_EXT_ERR_CERT_E_WRONG_USAGE IDS_EXT_ERR_CERT_E_UNTRUSTEDCA IDS_EXT_ERR_CERT_E_INVALID_POLICY IDS_EXT_ERR_CERT_E_INVALID_NAME IDS_EXT_ERR_CRYPT_E_MSG_ERROR IDS_EXT_ERR_CRYPT_E_UNKNOWN_ALGO IDS_EXT_ERR_CRYPT_E_OID_FORMAT IDS_EXT_ERR_CRYPT_E_INVALID_MSG_TYPE IDS_EXT_ERR_CRYPT_E_UNEXPECTED_ENCODING IDS_EXT_ERR_CRYPT_E_AUTH_ATTR_MISSING IDS_EXT_ERR_CRYPT_E_HASH_VALUE IDS_EXT_ERR_CRYPT_E_INVALID_INDEX IDS_EXT_ERR_CRYPT_E_ALREADY_DECRYPTED IDS_EXT_ERR_CRYPT_E_NOT_DECRYPTED IDS_EXT_ERR_CRYPT_E_RECIPIENT_NOT_FOUND IDS_EXT_ERR_CRYPT_E_CONTROL_TYPE IDS_EXT_ERR_CRYPT_E_ISSUER_SERIALNUMBER IDS_EXT_ERR_CRYPT_E_SIGNER_NOT_FOUND IDS_EXT_ERR_CRYPT_E_ATTRIBUTES_MISSING IDS_EXT_ERR_CRYPT_E_STREAM_MSG_NOT_READY IDS_EXT_ERR_CRYPT_E_STREAM_INSUFFICIENT_DATA IDS_EXT_ERR_CRYPT_I_NEW_PROTECTION_REQUIRED IDS_EXT_ERR_CRYPT_E_BAD_LEN IDS_EXT_ERR_CRYPT_E_BAD_ENCODE IDS_EXT_ERR_CRYPT_E_FILE_ERROR IDS_EXT_ERR_CRYPT_E_NOT_FOUND IDS_EXT_ERR_CRYPT_E_EXISTS IDS_EXT_ERR_CRYPT_E_NO_PROVIDER IDS_EXT_ERR_CRYPT_E_SELF_SIGNED IDS_EXT_ERR_CRYPT_E_DELETED_PREV IDS_EXT_ERR_CRYPT_E_NO_MATCH IDS_EXT_ERR_CRYPT_E_UNEXPECTED_MSG_TYPE IDS_EXT_ERR_CRYPT_E_NO_KEY_PROPERTY IDS_EXT_ERR_CRYPT_E_NO_DECRYPT_CERT IDS_EXT_ERR_CRYPT_E_BAD_MSG IDS_EXT_ERR_CRYPT_E_NO_SIGNER IDS_EXT_ERR_CRYPT_E_PENDING_CLOSE IDS_EXT_ERR_CRYPT_E_REVOKED IDS_EXT_ERR_CRYPT_E_NO_REVOCATION_DLL IDS_EXT_ERR_CRYPT_E_NO_REVOCATION_CHECK IDS_EXT_ERR_CRYPT_E_REVOCATION_OFFLINE IDS_EXT_ERR_CRYPT_E_NOT_IN_REVOCATION_DATABASE IDS_EXT_ERR_CRYPT_E_INVALID_NUMERIC_STRING IDS_EXT_ERR_CRYPT_E_INVALID_PRINTABLE_STRING IDS_EXT_ERR_CRYPT_E_INVALID_IA5_STRING IDS_EXT_ERR_CRYPT_E_INVALID_X500_STRING IDS_EXT_ERR_CRYPT_E_NOT_CHAR_STRING IDS_EXT_ERR_CRYPT_E_FILERESIZED IDS_EXT_ERR_CRYPT_E_SECURITY_SETTINGS IDS_EXT_ERR_CRYPT_E_NO_VERIFY_USAGE_DLL IDS_EXT_ERR_CRYPT_E_NO_VERIFY_USAGE_CHECK IDS_EXT_ERR_CRYPT_E_VERIFY_USAGE_OFFLINE IDS_EXT_ERR_CRYPT_E_NOT_IN_CTL IDS_EXT_ERR_CRYPT_E_NO_TRUSTED_SIGNER IDS_EXT_ERR_CRYPT_E_MISSING_PUBKEY_PARA IDS_EXT_ERR_CRYPT_E_OSS_ERROR IDS_EXT_ERR_CRYPT_E_ASN1_ERROR IDS_EXT_ERR_CRYPT_E_ASN1_INTERNAL IDS_EXT_ERR_CRYPT_E_ASN1_EOD IDS_EXT_ERR_CRYPT_E_ASN1_CORRUPT IDS_EXT_ERR_CRYPT_E_ASN1_LARGE IDS_EXT_ERR_CRYPT_E_ASN1_CONSTRAINT IDS_EXT_ERR_CRYPT_E_ASN1_MEMORY IDS_EXT_ERR_CRYPT_E_ASN1_OVERFLOW IDS_EXT_ERR_CRYPT_E_ASN1_BADPDU IDS_EXT_ERR_CRYPT_E_ASN1_BADARGS IDS_EXT_ERR_CRYPT_E_ASN1_BADREAL IDS_EXT_ERR_CRYPT_E_ASN1_BADTAG IDS_EXT_ERR_CRYPT_E_ASN1_CHOICE IDS_EXT_ERR_CRYPT_E_ASN1_RULE IDS_EXT_ERR_CRYPT_E_ASN1_UTF8 IDS_EXT_ERR_CRYPT_E_ASN1_PDU_TYPE IDS_EXT_ERR_CRYPT_E_ASN1_NYI IDS_EXT_ERR_CRYPT_E_ASN1_EXTENDED IDS_EXT_ERR_CRYPT_E_ASN1_NOEOD IDS_EXT_ERR_NTE_BAD_UID IDS_EXT_ERR_NTE_BAD_HASH IDS_EXT_ERR_NTE_BAD_KEY IDS_EXT_ERR_NTE_BAD_LEN IDS_EXT_ERR_NTE_BAD_DATA IDS_EXT_ERR_NTE_BAD_SIGNATURE IDS_EXT_ERR_NTE_BAD_VER IDS_EXT_ERR_NTE_BAD_ALGID IDS_EXT_ERR_NTE_BAD_FLAGS IDS_EXT_ERR_NTE_BAD_TYPE IDS_EXT_ERR_NTE_BAD_KEY_STATE IDS_EXT_ERR_NTE_BAD_HASH_STATE IDS_EXT_ERR_NTE_NO_KEY IDS_EXT_ERR_NTE_NO_MEMORY IDS_EXT_ERR_NTE_EXISTS IDS_EXT_ERR_NTE_PERM IDS_EXT_ERR_NTE_NOT_FOUND IDS_EXT_ERR_NTE_DOUBLE_ENCRYPT IDS_EXT_ERR_NTE_BAD_PROVIDER IDS_EXT_ERR_NTE_BAD_PROV_TYPE IDS_EXT_ERR_NTE_BAD_PUBLIC_KEY IDS_EXT_ERR_NTE_BAD_KEYSET IDS_EXT_ERR_NTE_PROV_TYPE_NOT_DEF IDS_EXT_ERR_NTE_PROV_TYPE_ENTRY_BAD IDS_EXT_ERR_NTE_KEYSET_NOT_DEF IDS_EXT_ERR_NTE_KEYSET_ENTRY_BAD IDS_EXT_ERR_NTE_PROV_TYPE_NO_MATCH IDS_EXT_ERR_NTE_SIGNATURE_FILE_BAD IDS_EXT_ERR_NTE_PROVIDER_DLL_FAIL IDS_EXT_ERR_NTE_PROV_DLL_NOT_FOUND IDS_EXT_ERR_NTE_BAD_KEYSET_PARAM IDS_EXT_ERR_NTE_FAIL IDS_EXT_ERR_NTE_SYS_ERR IDS_EXT_ERR_NTE_SILENT_CONTEXT IDS_EXT_ERR_NTE_TOKEN_KEYSET_STORAGE_FULL IDS_EXT_ERR_NTE_TEMPORARY_PROFILE IDS_EXT_ERR_NTE_FIXEDPARAMETER IDS_EXT_ERR_NTE_INVALID_HANDLE IDS_EXT_ERR_NTE_INVALID_PARAMETER IDS_EXT_ERR_NTE_BUFFER_TOO_SMALL IDS_EXT_ERR_NTE_NOT_SUPPORTED IDS_EXT_ERR_NTE_NO_MORE_ITEMS IDS_EXT_ERR_NTE_BUFFERS_OVERLAP IDS_EXT_ERR_NTE_DECRYPTION_FAILURE IDS_EXT_ERR_NTE_INTERNAL_ERROR IDS_EXT_ERR_NTE_UI_REQUIRED IDS_EXT_ERR_NTE_HMAC_NOT_SUPPORTED IDS_EXT_ERR_TRUST_E_SYSTEM_ERROR IDS_EXT_ERR_TRUST_E_NO_SIGNER_CERT IDS_EXT_ERR_TRUST_E_COUNTER_SIGNER IDS_EXT_ERR_TRUST_E_CERT_SIGNATURE IDS_EXT_ERR_TRUST_E_TIME_STAMP IDS_EXT_ERR_TRUST_E_BAD_DIGEST IDS_EXT_ERR_TRUST_E_BASIC_CONSTRAINTS IDS_EXT_ERR_TRUST_E_FINANCIAL_CRITERIA IDS_EXT_ERR_TRUST_E_PROVIDER_UNKNOWN IDS_EXT_ERR_TRUST_E_ACTION_UNKNOWN IDS_EXT_ERR_TRUST_E_SUBJECT_FORM_UNKNOWN IDS_EXT_ERR_TRUST_E_SUBJECT_NOT_TRUSTED IDS_EXT_ERR_TRUST_E_NOSIGNATURE IDS_EXT_ERR_TRUST_E_FAIL IDS_EXT_ERR_TRUST_E_EXPLICIT_DISTRUST IDS_EXT_ERR_SCARD_W_WRONG_CHV IDS_EXT_ERR_USER_CANCELLED IDS_EXT_ERR_SCARD_W_REMOVED_CARD IDS_EXT_ERR_CRYPTOPROVIDERDIFFERENT IDS_EXT_ERR_PRIVATEKEYLINKEDTO IDS_EXT_ERR_CHECKSIGNATUREFAILEDWITHNOTFOUND IDS_EXT_ERR_ENCRYPTIMPOSSIBLE IDS_EXT_ERR_SIGNIMPOSSIBLE IDS_EXT_ERR_FILEERROR IDS_EXT_ERR_CRYPTOGRAPHYNOTALLOWED IDS_TYPE_PRESENTATION_GEOGRAPHIC_COORDINATES IDS_EXT_ERR_MAIL_COMPOSE_MESSAGE IDS_EXT_ERR_MAIL_PARSE_MESSAGE IDS_EXT_ERR_MAIL_PARSE_SAVE_ATTACHMENT IDS_EXT_ERR_MAIL_UNKNOWN_ERROR IDS_EXT_ERR_DECLENSION_UNKNOWN IDS_EXT_ERR_DECLENSION_DICTIONARY IDS_EXT_ERR_DECLENSION_ENUM IDS_EXT_ERR_DECLENSION_PARAM IDS_EXT_ERR_DECLENSION_VALUE IDS_EXT_ERR_DECLENSION_SEQUENCE_BALANCE IDS_EXT_ERR_UNINITIALIZED IDS_PUSH_NOTIFICATION_IOS_SUBSCRIBER IDS_PUSH_NOTIFICATION_ANDROID_SUBSCRIBER IDS_PUSH_NOTIFICATION_WINRT_SUBSCRIBER IDS_PUSH_NOTIFICATION_ANDROID_SUBSCRIBERFCM IDS_PUSH_SOUNDALERT_NONE IDS_PUSH_SOUNDALERT_DEFAULT IDS_PUSH_AUTHENTICATIONDATA_ERROR IDS_PUSH_NOTIFICATIONBODY_ERROR IDS_PUSH_NOTIFICATIONSLIMITEXCEEDED_ERROR IDS_PUSH_DELIVERABLENOTIFICATIONSUBSCRIBERID_ERROR IDS_PUSH_PUSHSERVICEPROVIDERCONNECTION_ERROR IDS_PUSH_PUSHSERVICEPROVIDER_ERROR IDS_PUSH_UNKNOWN_ERROR IDS_EXCEPTION_PAYLOADTOOLARGE IDS_EXCEPTION_BADAUTHENTICATION IDS_EXCEPTION_NOTIFICATIONTEXTEMPTY IDS_EXCEPTION_NOTIFICATIONRECEIVERSEMPTY IDS_EXCEPTION_BADSERTIFICATE IDS_EXCEPTION_SETUPSSL IDS_EXCEPTION_APNS_SEND IDS_EXCEPTION_GCM_SEND IDS_EXCEPTION_WNS_SEND IDS_EXCEPTION_SEND_UNAVAILABLE IDS_EXCEPTION_SEND_MISMATCHSENDERID IDS_EXCEPTION_SEND_REGISTRATION IDS_EXCEPTION_SEND_APNS1 IDS_EXCEPTION_SEND_APNS2 IDS_EXCEPTION_SEND_APNS3 IDS_EXCEPTION_SEND_APNS4 IDS_EXCEPTION_SEND_APNS5 IDS_EXCEPTION_SEND_APNS6 IDS_EXCEPTION_SEND_APNS7 IDS_EXCEPTION_SEND_SERVICE_SENDER IDS_EXCEPTION_SEND_SERVICE_RECIPIENT IDS_EXCEPTION_SEND_SERVICE_RIGHT IDS_EXCEPTION_SEND_SERVICE_TOKENS IDS_EXCEPTION_SEND_SERVICE_ERROR IDS_EXCEPTION_VERIFY_SEND IDS_EXCEPTION_OBJECTISNOTNOTIFICATION IDS_EXCEPTION_SUBSCRIBERIDERROR IDS_EXCEPTION_BADAUTHENTICATIONTOKEN IDS_EXCEPTION_BADPUSHNOTIFICATIONCHANNEL IDS_EXCEPTION_NOTIFICATIONSLIMITEXCEEDED IDS_EXCEPTION_PUSHNOTIFICATIONCHANNELEXPIRED IDS_EXCEPTION_NOTIFICATIONPROVIDERERROR IDS_EXCEPTION_NOTIFICATIONPROVIDERUNAVALIABLE IDS_EXCEPTION_INVALIDTOKEN IDS_EXCEPTION_PROTOCOLERROR IDS_EXCEPTION_UNKNOWNERROR IDS_TYPE_PRESENTATION_BINARY_DATA_BUFFER IDS_TYPE_PRESENTATION_BINARY_DATA_BUFFER_MANAGER IDS_TYPE_PRESENTATION_BINARY_DATA_MANAGER IDS_TYPE_PRESENTATION_STREAM IDS_TYPE_PRESENTATION_FILE_STREAM IDS_TYPE_PRESENTATION_FILE_STREAM_MANAGER IDS_TYPE_PRESENTATION_DATA_READER IDS_TYPE_PRESENTATION_READ_DATA_RESULT IDS_TYPE_PRESENTATION_DATA_WRITER IDS_TYPE_PRESENTATION_BINARY_DATA_CONVERTER IDS_BUFFER_SIZE_CANT_BE_ZERO IDS_BYTE_ORDER_LITTLE_ENDIAN IDS_BYTE_ORDER_BIG_ENDIAN IDS_SEEK_ORIGIN_BEGIN IDS_SEEK_ORIGIN_CURRENT IDS_SEEK_ORIGIN_END IDS_FILE_ACCESS_READ IDS_FILE_ACCESS_WRITE IDS_FILE_ACCESS_READ_AND_WRITE IDS_FILE_OPEN_MODE_CREATE IDS_FILE_OPEN_MODE_CREATE_NEW IDS_FILE_OPEN_MODE_OPEN IDS_FILE_OPEN_MODE_OPEN_OR_CREATE IDS_FILE_OPEN_MODE_TRUNCATE IDS_FILE_OPEN_MODE_APPEND IDS_ERROR_BUFFER_OVERFLOW IDS_ERROR_BUFFER_READONLY IDS_ERROR_BUFFER_UNKNOWN IDS_CANT_CREATE_BIN_DATA_BUFFER_AS_RESULT_EXCEPTION IDS_ERROR_COUNT_CANNOT_BE_NEGATIVE IDS_ERROR_BYTE_RANGE_MISMATCH IDS_ERROR_INTEGER_RANGE_MISMATCH IDS_CANT_WRITE_STREAM_EXCEPTION IDS_CANT_READ_STREAM_EXCEPTION IDS_CANT_SEEK_STREAM_EXCEPTION IDS_CREATE_STREAM_EXCEPTION IDS_STREAM_CLOSED_EXCEPTION IDS_STREAM_CANT_READ_VALUE_EXCEPTION IDS_STREAM_NOT_EXISTS IDS_FILE_NOT_FOUND_EXCEPTION IDS_DIRECTORY_NOT_FOUND_EXCEPTION IDS_INVALID_FILE_PATH_EXCEPTION IDS_FILE_ACCESS_EXCEPTION IDS_FILE_SHARE_EXCEPTION IDS_DEVICE_FULL_EXCEPTION IDS_FILE_ALREADY_EXISTS_EXCEPTION IDS_GENERIC_FILE_EXCEPTION IDS_FILE_CREATE_NO_WRITE_ACCESS IDS_FILE_WRITE_NO_WRITE_ACCESS IDS_ERROR_DATAREADER_POSITION_CHANGED IDS_ERROR_DATAWRITER_POSITION_CHANGED IDS_MOBILEPROPNOTAVAILABLE IDS_MOBILEMETHNOTAVAILABLE IDS_FILENAMES_IN_ZIPFILE_AUTO IDS_FILENAMES_IN_ZIPFILE_CURRENTOS IDS_FILENAMES_IN_ZIPFILE_UTF8 IDS_FSE_OPEN_FILE_READ IDS_FSE_OPEN_FILE_WRITE IDS_FSE_OPEN_FILE_READ_WRITE IDS_FSE_CREATE_TEMP_FILE IDS_FSE_OPEN_FILE_READ2 IDS_FSE_OPEN_FILE_WRITE2 IDS_FSE_OPEN_FILE_READ_WRITE2 IDS_FSE_CREATE_TEMP_FILE2 IDS_FERROR_ACCESS IDS_FERROR_BADPATH IDS_FERROR_CLOSED IDS_FERROR_DEVICEFULL IDS_FERROR_DIRNOTFOUND IDS_FERROR_FILEEXISTS IDS_FERROR_FILENOTFOUND IDS_FERROR_LOCK IDS_FERROR_READONLY IDS_FERROR_FAILURE MsgMarkers.png MsgMarkers_old.png ext.xsd model.xdto pl2.spl prd_ru.spl prd_en.spl prd_uk.spl prd_be.spl prd_lv.spl prd_fi.spl prd_et.spl prd_bg.spl prd_de.spl prd_kk.spl prd_lt.spl prd_ro.spl prd_ka.spl prd_pl.spl prd_vi.spl prd_tr.spl prd_fr.spl prd_az.spl prd_tk.spl prd_hu.spl plurals.txt ���  F C M   W N S   G C M   E n d   A u t o   O p e n   A P N s   F i l e   R e a d   N o n e   W r i t e   X B a s e   R a n g e   T o d a y   S i n g l e   S t r e a m   A l w a y s   A p p e n d   C r e a t e   D e f a u l t   P r o c e s s   C u r r e n t   T r u n c a t e   M u l t i p l e   B e g i n n i n g   X B a s e   k e y   C r e a t e   n e w   B i g - e n d i a n   F i l e   s t r e a m   D a t a   r e a d e r   X B a s e   i n d e x   D a t a   w r i t e r   X B a s e   f i e l d   I n v a l i d   n a m e   S a v e   Z I P   f i l e   U n k n o w n   v a l u e   R e s t o r e   p a t h s   R e a d   Z I P   f i l e   U n k n o w n   e r r o r   S e r v i c e   e r r o r   L i t t l e - e n d i a n   U n k n o w n   i n d e x   I n v a l i d   i n d e x   M i s s i n g   t o p i c   U n k n o w n   r e s u l t   F i l e   n o t   f o u n d   C h e c k s u m   e r r o r   O p e n   o r   c r e a t e   F i l e   n o t   o p e n .   F i l e   r e f e r e n c e   P a t h   n o t   f o u n d   D o   n o t   p r o c e s s   R e a d   a n d   w r i t e   B u f f e r   o v e r f l o w   M e s s a g e   t o   u s e r   T e x t   e x t r a c t i o n   M i s s i n g   p a y l o a d   S a v e   f u l l   p a t h s   V o l u m e   n o t   f o u n d   U n k n o w n   f u n c t i o n   I n v a l i d   p a s s w o r d   P r o c e s s i n g   e r r o r   I n c o m p l e t e   r u l e .   M i s s i n g   t y p e   I D .   N o   m o r e   e l e m e n t s   U n k n o w n   o p e r a t o r   K e y   i s   n o t   u n i q u e   I n v a l i d   f i l e   p a t h   I n v a l i d   f i l e   n a m e   F i l e   a c c e s s   e r r o r   I n v a l i d   p a r a m e t e r   D o   n o t   s a v e   p a t h s   U n k n o w n   c a t e g o r y .   Z I P   f i l e   e l e m e n t s   I n v a l i d   t o k e n   s i z e   T h e   d e v i c e   i s   f u l l   O p t i m i z a t i o n   e r r o r   H T M L   p a r s i n g   e r r o r   E r r o r   c l o s i n g   f i l e   U n k n o w n   m a i l   e r r o r   E r r o r   o p e n i n g   f i l e   I n v a l i d   i n d e x   n a m e   U n k n o w n   f i e l d   t y p e   D a t a   r e a d e r   r e s u l t   U n k n o w n   l o c a l e   I D .   U n k n o w n   f i e l d   n a m e   I n v a l i d   c h a r a c t e r .   F i l e   a c c e s s   d e n i e d   E r r o r   l o c k i n g   f i l e   I n v a l i d   f i e l d   n a m e   Z I P   2 . 0   e n c r y p t i o n   I n v a l i d   i n d e x   f i l e   I n v a l i d   t o p i c   s i z e   B i n a r y   d a t a   b u f f e r   B i n a r y   d a t a   m a n a g e r   I n v a l i d   h a s h   v a l u e .   I n v a l i d   f i l e   f o r m a t   I n s u f f i c i e n t   m e m o r y   S u b s c r i b e r   I D   e r r o r   F i l e   s t r e a m   m a n a g e r   I n v a l i d   i n f o r m a t i o n   E r r o r   r e n a m i n g   f i l e   E r r o r   r e a d i n g   t e x t .   F i l e   n a m e   n o t   f o u n d   F i l e   a l r e a d y   e x i s t s   S a v e   r e l a t i v e   p a t h s   D i r e c t o r y   n o t   f o u n d   L i n e   n o t   t e r m i n a t e d   E r r o r   d e l e t i n g   f i l e   E r r o r   c o p y i n g   f i l e s   E r r o r   c r e a t i n g   f i l e   I n v a l i d   e x p r e s s i o n .   E r r o r   w r i t i n g   t e x t .   E r r o r   u n l o c k i n g   f i l e   D o   n o t   r e s t o r e   p a t h s   I n v a l i d   f i e l d   n u m b e r   A r c h i v e   i s   n o t   o p e n !   E r r o r   r e n a m i n g   f i l e s   M i s s i n g   d e v i c e   t o k e n   I n v a l i d   a c c e s s   t o k e n   I n v a l i d   n u m b e r   v a l u e   E r r o r   d e l e t i n g   f i l e s   M e t h o d   n o t   a v a i l a b l e   F i l e   o p e r a t i o n   e r r o r   C r y p t o g r a p h y   m a n a g e r   M e t h o d   n o t   s u p p o r t e d   I n v a l i d   i n d e x   v a l u e .   U n k n o w n   b u f f e r   e r r o r   E r r o r   s p l i t t i n g   f i l e   U n k n o w n   t e x t   f o r m a t .   I n v a l i d   p a y l o a d   s i z e   M i s m a t c h e d   s e n d e r   I D   B i n a r y   d a t a   c o n v e r t e r   O S   e n c o d i n g   w i t h   U T F 8   E r r o r   a s s e m b l i n g   f i l e   C e r t i f i c a t e   r e c a l l e d .   T h e   f i l e   i s   n o t   f o u n d   I n c o m p l e t e   e x p r e s s i o n   E r r o r   w r i t i n g   t o   f i l e   G e o g r a p h i c   c o o r d i n a t e s   E l e m e n t   a l r e a d y   e x i s t s   N o   r e c i p i e n t   s e l e c t e d .   E r r o r   a c c e s s i n g   f i l e :     A E S   1 2 8   b i t   e n c r y p t i o n   A E S   2 5 6   b i t   e n c r y p t i o n   A E S   1 9 2   b i t   e n c r y p t i o n   X B a s e   i n d e x   c o l l e c t i o n   X B a s e   f i e l d   c o l l e c t i o n   E r r o r   s e e k i n g   t h e   f i l e   E r r o r   r e a d i n g   f r o m   f i l e   C a n n o t   r e a d   s t r e a m   d a t a   B Z I P   c o m p r e s s i o n   m e t h o d   N o t i f i c a t i o n   b o d y   e r r o r   C o p y   c o m p r e s s i o n   m e t h o d   F i l e   s e g m e n t   l o c k   e r r o r   A t t a c h m e n t   s a v i n g   e r r o r   S e r v e r   n e t w o r k i n g   e r r o r   O b j e c t   n o t   i n i t i a l i z e d .   I n s u f f i c i e n t   d i s k   s p a c e   F i l e   f o r m a t   i s   n o t   . D B F   A r c h i v e   i s   a l r e a d y   o p e n !   S t a r t i n g   t h e   a p p l i c a t i o n   E r r o r   o f   f i l e   o p e r a t i o n .   S h a r e d   f i l e   a c c e s s   e r r o r   M e s s a g e   g e n e r a t i o n   e r r o r   U n k n o w n   d e c l e n s i o n   e r r o r   I n v a l i d   m a n a g e m e n t   t y p e .   O n   s u c c e s s f u l   c o m p l e t i o n   I n v a l i d   p a s s e d   p a r a m e t e r   C r y p t o g r a p h y   c e r t i f i c a t e   E r r o r   c r e a t i n g   d i r e c t o r y   E r r o r   s e a r c h i n g   f o r   f i l e s   S t r e a m   c r e a t i o n   e r r o r :   % s   D e l i v e r a b l e   n o t i f i c a t i o n     T h e   s t r e a m   d o e s   n o t   e x i s t   R e a d   o p e r a t i o n   t i m e d   o u t .   U n r e g i s t e r e d   d e v i c e   t o k e n   A u t h e n t i c a t i o n   d a t a   e r r o r   E r r o r   s e t t i n g   f i l e   l e n g t h   N o   r e c i p i e n t  s   s i g n a t u r e .   W r o n g   n u m b e r   o f   p a r a m e t e r s   D e s c r i p t i o n   o f   f i l e   p a s s e d   U n s u p p o r t e d   f i l t e r   k e y :   % s   T h e   d i r e c t o r y   i s   n o t   f o u n d   S M T P   e r r o r .   E r r o r   c o d e :   % s   I n v a l i d   s u b e x p r e s s i o n   t y p e   P O P 3   e r r o r .   E r r o r   c o d e :   % s   O p e r a t i o n   c a n c e l e d   b y   u s e r   R e c e i p t   v e r i f i c a t i o n   e r r o r   E r r o r   g e t t i n g   C O M   o b j e c t :     B i n a r y   d a t a   b u f f e r   m a n a g e r   E r r o r   i n   t h e   f o r m a t   s t r i n g   C r y p t o g r a p h y   t o o l s   m a n a g e r   U n a b l e   t o   d e t e r m i n e   s i g n e r .   S p e c i f y   P O P 3   s e r v e r   a d d r e s s   S p e c i f y   S M T P   s e r v e r   a d d r e s s   T h e   b u f f e r   s i z e   c a n n o t   b e   0   I n v a l i d   c e r t i f i c a t e   p o l i c y .     -   C r e a t e   a   t e m p o r a r y   f i l e .   C u r r e n t   i n d e x   n o t   s p e c i f i e d   I n v a l i d   p a t h   f o r   e x t r a c t i o n   R e f e r e n c e   t o   d e l e t e d   o b j e c t   C a n n o t   a c c e s s   a   c l o s e d   f i l e   D e s c r i p t i o n   o f   f i l e   p a s s i n g   I n s u f f i c i e n t   s p a c e   f o r   s a v e   D e f l a t i o n   c o m p r e s s i o n   m e t h o d   N o t i f i c a t i o n s   l i m i t   e x c e e d e d   E r r o r   d i s a b l i n g   o p t i m i z a t i o n     -   O p e n   f i l e   % s   f o r   r e a d i n g .   M i n i m u m   l e v e l   o f   c o m p r e s s i o n   O p e r a t i o n   c a n c e l l e d   b y   u s e r .   O p t i m u m   l e v e l   o f   c o m p r e s s i o n     -   O p e n   f i l e   % s   f o r   w r i t i n g .   M a x i m u m   l e v e l   o f   c o m p r e s s i o n   T h e   p r o p e r t y   i s   n o t   a v a i l a b l e   E r r o r   d e t e r m i n i n g   f i l e   l e n g t h   G e n e r i c   a u t h e n t i c a t i o n   e r r o r .   C a n n o t   a c c e s s   a   c l o s e d   s t r e a m   C l o s i n g   p a r e n t h e s i s   i s   m i s s i n g   C o n v e r s i o n   a l g o r i t h m   n o t   f o u n d   C o m m a   o r   p a r e n t h e s i s   e x p e c t e d     D a t a b a s e   s t r u c t u r e   n o t   d e f i n e d   F i l e   r e a d i n g   o r   w r i t i n g   e r r o r .   O v e r f l o w   c a l c u l a t i n g   e x p r e s s i o n   C r y p t o g r a p h y   m o d u l e   i n f o r m a t i o n   O p e r a t i o n   c o m p l e t e d   w i t h   e r r o r s   D a t a   e n c r y p t i o n   e r r o r   ( 0 x % 0 8 X ) .   E r r o r   e x t r a c t i n g   t e x t   f r o m   f i l e   T h e   n o t i f i c a t i o n   t e x t   i s   e m p t y .   I M A P   s e r v e r   a d d r e s s   i s   r e q u i r e d   U n k n o w n   c r y p t o g r a p h y   a l g o r i t h m .   U n k n o w n   c e r t i f i c a t i o n   a u t h o r i t y .   U n s u p p o r t e d   f i l t e r   k e y   v a l u e :   % s   I n v a l i d   c r y p t o g r a p h i c   d a t a   t y p e .   C a n n o t   w r i t e   t o   a   r e a d - o n l y   f i l e   C e r t i f i c a t e   e r r o r :   i n v a l i d   d a t e .   P u s h   n o t i f i c a t i o n   c h a n n e l   e x p i r e d   S o u r c e   m e s s a g e   t e x t   p a r s i n g   e r r o r   P u s h   n o t i f i c a t i o n s   l i m i t   e x c e e d e d   M a x i m u m   i n p u t   d a t a   s i z e   e x c e e d e d .   N o   r e c i p i e n t   i n   t h e   m e s s a g e   d a t a .   E r r o r   c h e c k i n g   c e r t i f i c a t e   c h a i n .   E r r o r   d e f i n i n g   a l g o r i t h m   ( 0 x % 0 8 X ) .   U n k n o w n   e r r o r   w o r k i n g   w i t h   a r c h i v e   O p e r a t i o n   c o m p l e t e d   w i t h o u t   e r r o r s   C a n n o t   w r i t e   t o   a   r e a d - o n l y   b u f f e r   C o m m a n d - l i n e   i n t e r p r e t e r   n o t   f o u n d   P u s h   n o t i f i c a t i o n   s e n d i n g   m a n a g e r     S e l f   s i g n e d   c e r t i f i c a t e   s p e c i f i e d .   U n e x p e c t e d   e r r o r   i n   a r c h i v e   m a n a g e r   N o t i f i c a t i o n   d e l i v e r y   s e r v i c e   e r r o r   E r r o r   c o n v e r t i n g   W C S   s t r i n g   t o   M B S .   E r r o r   c o n v e r t i n g   M B S   s t r i n g   t o   W C S .   L i s t   o f   a d d r e s s e s   n o t   u s e d   b y   p r o x y   E r r o r   l o a d i n g   d y n a m i c   l i n k   l i b r a r y .   T h e   s e r v e r   r e j e c t e d   t h e   c o n n e c t i o n .   I n t e r n a l   I M A P   e r r o r .   E r r o r   c o d e :   % s   T h e   O A u t h   2   t o k e n   i s   n o t   s p e c i f i e d .   E r r o r   c r e a t i n g   c e r t i f i c a t e   c o n t e x t .   C r y p t o g r a p h y   c e r t i f i c a t e s   r e p o s i t o r y   R e c o r d   l e n g t h   e x c e e d s   a l l o w a b l e   s i z e   I n v a l i d   i s s u e r   a n d / o r   s e r i a l   n u m b e r .   6 4 - b i t   d e f l a t i o n   c o m p r e s s i o n   m e t h o d     I n v a l i d   c r y p t o g r a p h i c   d a t a   e n c o d i n g .   C e r t i f i c a t e   r e p o s i t o r y   i s   r e a d   o n l y .   E r r o r   d e c r y p t i n g   c r y p t o g r a p h i c   d a t a .   S i g n a t u r e   g e n e r a t i o n   e r r o r   ( 0 x % 0 8 X ) .   N o n - n u m e r i c   c h a r a c t e r   i n   t h e   s t r i n g .   T h e   i n p u t   p a r a m e t e r   i s   n o t   a   n u m b e r .   F i l e   n a m e   l e n g t h   e x c e e d s   8   c h a r a c t e r s   T h e   b y t e   v a l u e   m u s t   b e   i n   r a n g e   0 - 2 5 5   U n a b l e   t o   l o c a t e   t h e   r e q u i r e d   o b j e c t .   S e n d   o p e r a t i o n   w a i t   t i m e   h a s   e x p i r e d .   E r r o r   e s t a b l i s h i n g   S S L   c o n n e c t i o n :   % s   E r r o r   w r i t i n g   s e r v e r   a d d r e s s   ( _ c o d e _ )   S y s t e m - l e v e l   e r r o r   i n   a u t h e n t i c a t i o n .   E r r o r   o p e n i n g   c e r t i f i c a t e   r e p o s i t o r y .   E r r o r   r e a d i n g   c r y p t o g r a p h i c   d a t a   ( 1 ) .   E r r o r   r e a d i n g   c r y p t o g r a p h i c   d a t a   ( 2 ) .   U n k n o w n   e r r o r   ( S M T P   e r r o r   c o d e   _ c o d e _ )   S i g n a t u r e   v e r i f i c a t i o n   e r r o r   ( 0 x % 0 8 X ) .   C o u l d n ' t   f i n d   t h e   i n d e x   w i t h   t h i s   n a m e   E r r o r   i n   c a l c u l a t i o n   o b j e c t   p a r a m e t e r s   I n s u f f i c i e n t   m e m o r y   t o   e x e c u t e   c o m m a n d   U n k n o w n   e r r o r   e x e c u t i n g   s y s t e m   c o m m a n d   T h e   o b j e c t   o r   p r o p e r t y   a l r e a d y   e x i s t s .   E r r o r   s e n d i n g   m e s s a g e   # % d   o v e r   G C M :   % s   I n s u f f i c i e n t   r i g h t s   f o r   c a l l i n g   m e t h o d   T h e   i s   a   f i l e   w i t h   t h i s   n a m e   i n   a r c h i v e   E r r o r   e x e c u t i n g   c r y p t o g r a p h y   o p e r a t i o n .   M a x i m u m   c e r t i f i c a t e   f i l e   s i z e   e x c e e d e d .   O b j e c t   p a s s e d   i s   n o t   a   Z I P   f i l e   e l e m e n t   E r r o r   s e n d i n g   m e s s a g e   # % d   o v e r   A P N s :   % s   F i l e   p a t h   l e n g t h   e x c e e d s   2 5 5   c h a r a c t e r s   D o   y o u   w a n t   t o   c r e a t e   a   t e m p o r a r y   f i l e ?   E r r o r   g e t t i n g   c e r t i f i c a t e   c h a i n   c o n t e x t .   U n a b l e   t o   l o c a t e   t h e   o b j e c t   o r   p r o p e r t y .   T h e   n o t i f i c a t i o n   s i z e   e x c e e d s   t h e   l i m i t .   D o   y o u   w a n t   t o   o p e n   f i l e   % s   f o r   r e a d i n g ?   D o   y o u   w a n t   t o   o p e n   f i l e   % s   f o r   w r i t i n g ?   I n v a l i d   f o r m a t t i n g   o f   o b j e c t   i d e n t i f i e r .   T h e   P O P 3   s e r v e r   r e j e c t e d   t h e   c o n n e c t i o n .   R e q u i r e d   p e r s o n a l   c e r t i f i c a t e   n o t   f o u n d .     -   O p e n   f i l e   % s   f o r   r e a d i n g   a n d   w r i t i n g .   )�C e r t i f i c a t e   n o t   c o n n e c t e d   t o   p r i v a c y   k e y .   )�A   r u l e   f o r   c a r d i n a l   n u m b e r s   i s   n o t   f o u n d .   )�E r r o r   c r e a t i n g   c e r t i f i c a t e   c h a i n   c o n t e x t .   )�S p e c i f y   a t   l e a s t   o n e   m a i l   s e r v e r   a d d r e s s .   )�S i g n a t u r e   d a t a   g e n e r a t i o n   e r r o r   ( 0 x % 0 8 X ) .   )�I n c o r r e c t   c o m m a n d - l i n e   i n t e r p r e t e r   f o r m a t   )�I n v a l i d   l e n g t h   s p e c i f i e d   f o r   o u t p u t   d a t a .   )�C e r t i f i c a t e   e x p l i c i t l y   r e v o k e d   b y   i s s u e r .   *�T h e   r e c e i v e r   I D   s h o u l d   b e   r e p l a c e d   w i t h   % s   *�P r o t e c t e d   d a t a   n e e d   t o   b e   p r o t e c t e d   a g a i n .   *�N o t i f i c a t i o n   r e c i p i e n t s   a r e   n o t   s p e c i f i e d .   *�E r r o r   g e t t i n g   c r y p t o g r a p h y   m o d u l e   c o n t e x t .   *�T h e   " % s "   p a r a m e t e r   v a l u e   i s   n o t   s u p p o r t e d .   *�P u s h   n o t i f i c a t i o n   s e r v i c e   i s   n o t   a v a i l a b l e   +�L o c a l   s e r v e r   e r r o r   ( S M T P   e r r o r   c o d e   _ c o d e _ )   +�I I F ( )   r e q u i r e s   p a r a m e t e r   o f   t h e   s a m e   l e n g t h   +�C a n n o t   e s t a b l i s h   s e c u r e   S S L / T L S   c o n n e c t i o n .   ,�E r r o r   g e t t i n g   c r y p t o g r a p h y   m o d u l e   p a r a m e t e r .   ,�E r r o r   g e t t i n g   v a l u e   f r o m   c r y p t o g r a p h i c   d a t a .   ,�T h e   o b j e c t   i s   n o t   a   d e l i v e r a b l e   n o t i f i c a t i o n   ,�D a t a   n o t   e n c r y p t e d   o r   a r e   i n   i n v a l i d   f o r m a t .   ,�E r r o r   i n   e n c r y p t i o n   o r   d e c r y p t i o n   o p e r a t i o n .   ,�A t   l e a s t   o n e   o f   t h e   s i g n a t u r e s   i s   n o t   v a l i d .   ,�E r r o r   s e t t i n g   c r y p t o g r a p h y   m o d u l e   p a r a m e t e r .   ,�E r r o r   g e t t i n g   c e r t i f i c a t e   p r o p e r t y   ( 0 x % 0 8 X ) .   ,�C r y p t o g r a p h y   m o d u l e   i n t e r f a c e   e r r o r .   N o   k e y .   ,�I n s u f f i c i e n t   m e m o r y   t o   p e r f o r m   t h e   o p e r a t i o n   ,�C e r t i f i c a t e   s i g n a t u r e   c o u l d   n o t   b e   v e r i f i e d .   ,�C O M - o b j e c t s   a r e   o n l y   s u p p o r t e d   i n   W i n d o w s   O S   -�N o   v e n d o r   s p e c i f i e d   f o r   r e p o s i t o r y   o r   o b j e c t .   -�P r i v a c y   k e y   m e d i u m   i s   m i s s i n g   o r   u n a v a i l a b l e .   -�E r r o r   d e c r y p t i n g   c r y p t o g r a p h i c   d a t a   ( 0 x % 0 8 X ) .   -�C a n n o t   c o n n e c t   t o   I M A P   s e r v e r .   E r r o r   c o d e :   % s   -�C r y p t o g r a p h i c   d a t a   c o n t e n t   a l r e a d y   d e c r y p t e d .   -�C r y p t o g r a p h i c   d a t a   c o n t e n t   n o t   d e c r y p t e d   y e t .   .�E r r o r   o p e n i n g   c e r t i f i c a t e   r e p o s i t o r y   ( 0 x % 0 8 X ) .   .�C e r t i f i c a t e   n o t   i n t e n d e d   f o r   t h e   s p e c i f i e d   u s e   .�N o t i f i c a t i o n   d e l i v e r y   s e r v i c e   c o n n e c t i o n   e r r o r   .�M a i l b o x   n o t   a v a i l a b l e   ( S M T P   e r r o r   c o d e   _ c o d e _ )   .�T h e   d i r e c t o r y   n a m e   c o n t a i n s   i n v a l i d   c h a r a c t e r s   .�A u t h e n t i c a t i o n   f a i l e d   ( S M T P   e r r o r   c o d e   _ c o d e _ )   .�N o t i f i c a t i o n   d e l i v e r y   s e r v i c e   i s   n o t   a v a i l a b l e   .�C e r t i f i c a t e   i s s u e r   h a s   n o t   a c t u a l l y   i s s u e d   i t .   /�S M T P   e r r o r .   E r r o r   c o d e :   % s .   S e r v e r   r e s p o n s e :   % s   /�S U B S T R ( )   a n d   S T R ( )   r e q u i r e   a   c o n s t a n t   p a r a m e t e r   /�C r y p t o g r a p h y   m o d u l e   i n t e r f a c e   e r r o r .   N o   k e y s e t .   /�P O P 3   e r r o r .   E r r o r   c o d e :   % s .   S e r v e r   r e s p o n s e :   % s   /�S p e c i f i e d   e n c o d i n g   ( _ c h a r s e t _ )   i s   n o t   s u p p o r t e d   /�S t r e a m   c r y p t o g r a p h i c   d a t a   n o t   r e a d y   f o r   o u t p u t .   /�X B a s e   d o e s   n o t   s u p p o r t   w o r k i n g   w i t h   m e m o   f i e l d s   /�A u t h e n t i c a t i o n   e r r o r .   S e r v e r   r e p l i e s :   " _ r e p l y _ "   0�T e m p o r a r y   s e r v e r   e r r o r .   ( S M T P   e r r o r   c o d e   _ c o d e _ )   0�A   n o n - s t r i n g   t y p e   s p e c i f i e d   f o r   C E R T _ N A M E _ V A L U E .   0�D i g i t a l   s i g n a t u r e   o f   t h e   o b j e c t   i s   n o t   v e r i f i e d .   0�B e f o r e   p e r f o r m i n g   o p e r a t i o n ,   s e t   o b j e c t   t o   w r i t e   1�T h e   m a i l   s e r v e r   d o e s   n o t   s u p p o r t   m e s s a g e   m o v e m e n t   1�T h e   n u m b e r   o f   e l e m e n t s   o r   s i z e   c a n n o t   b e   n e g a t i v e   1�C r y p t o g r a p h y   m o d u l e   i n t e r f a c e   e r r o r .   I n v a l i d   U I D .   1�C r y p t o g r a p h y   m o d u l e   i n t e r f a c e   e r r o r .   I n v a l i d   k e y .   1�C a n n o t   p a r s e   I M A P   s e r v e r   r e s p o n s e .   E r r o r   c o d e :   % s   2�C e r t i f i c a t e   o f   t h e   s i g n e r   i s   i n v a l i d   o r   n o t   f o u n d .   2�P a t h   l e n g t h   l i m i t   e x c e e d e d   i n   c e r t i f i c a t i o n   c h a i n .   2�M a x i m u m   n u m b e r   o f   r e c o r d s   i n   d e m o   v e r s i o n   e x c e e d e d   2�N o   c e r t i f i c a t e   i n   p e r s o n a l   c e r t i f i c a t e   r e p o s i t o r y .   2�C r y p t o g r a p h y   m o d u l e   i n t e r f a c e   e r r o r .   A c c e s s   e r r o r .   2�C r y p t o g r a p h y   m o d u l e   i n t e r f a c e   e r r o r .   I n v a l i d   d a t a .   2�C r y p t o g r a p h y   m o d u l e   i n t e r f a c e   e r r o r .   I n v a l i d   h a s h .   2�T e x t   c o n t a i n s   u n p a i r e d   b r a c k e t s   o r   q u o t a t i o n   m a r k s   2�I n v a l i d   n u m b e r   o f   c a t e r o g i e s   i n   t h e   s o u r c e   s t r i n g .   2�C e r t i f i c a t e   e n c r y p t i n g / d e c r y p t i n g   e r r o r   c o d e   b a s e .   3�C e r t i f i c a t e   v a l i d a t i o n   e r r o r :   u n k n o w n   e r r o r   0 x % 0 8 X .   3�C r y p t o g r a p h y   o p e r a t i o n   e r r o r :   u n k n o w n   e r r o r   0 x % 0 8 X .   3�E r r o r   g e t t i n g   c r y p t o g r a p h y   m o d u l e   c o n t e x t   ( 0 x % 0 8 X ) .   3�C r y p t o g r a p h y   m o d u l e   i n t e r f a c e   e r r o r .   I n v a l i d   f l a g s .   3�P a r a m e t e r s   o f   t h e   p u b l i c   k e y   a l g o r i t h m   a r e   o m i t t e d .   3�E r r o r   r e a d i n g   c e r t i f i c a t e s   f r o m   s i g n a t u r e   ( 0 x % 0 8 X ) .   4�C r y p t o g r a p h y   m o d u l e   i n t e r f a c e   e r r o r .   I n v a l i d   h a n d l e .   4�E r r o r   g e t t i n g   c e r t i f i c a t e s   f r o m   r e p o s i t o r y   ( 0 x % 0 8 X ) .   4�I n d e x   m u s t   b e   s e l e c t e d   b e f o r e   o p e r a t i o n   i s   p e r f o r m e d   4�D o   y o u   w a n t   t o   o p e n   f i l e   % s   f o r   r e a d i n g   a n d   w r i t i n g ?   4�C u r r e n t   g l o s s a r y   i s   c o r r u p t e d   o r   h a s   i n v a l i d   f o r m a t .   4�T h e   s e l e c t e d   e n u m e r a t i o n   d o e s   n o t   c o n t a i n   t h i s   v a l u e   4�C r y p t o g r a p h y   m o d u l e   i n t e r f a c e   e r r o r .   I n t e r n a l   e r r o r .   4�I n c o r r e c t   f i e l d   l e n g t h   s p e c i f i e d   f o r   t h i s   f i e l d   t y p e   5�C r y p t o g r a p h y   m o d u l e   i n t e r f a c e   e r r o r .   L o w - l e v e l   e r r o r .   5�D a t a b a s e   m u s t   b e   o p e n e d   b e f o r e   o p e r a t i o n   i s   p e r f o r m e d   5�C a n n o t   s e e k   i n   a   s t r e a m   t h a t   d o e s   n o t   s u p p o r t   s e e k i n g   5�U n a b l e   t o   v e r i f y   i f   u s a g e   b y   t h e   s u b j e c t   i s   p o s s i b l e .   5�D a t a b a s e   m u s t   b e   c l o s e d   b e f o r e   o p e r a t i o n   i s   p e r f o r m e d   6�I n v a l i d   o v e r l a p   o f   c e r t i f i c a t e   c h a i n   v a l i d i t y   p e r i o d s .   6�A l l o c a t e d   d i s k   s p a c e   e x c e e d e d   ( S M T P   e r r o r   c o d e   _ c o d e _ )   6�N o   s i g n e r   w i t h   t h e   s p e c i f i e d   i n d e x   i n   t h e   s i g n e d   d a t a .   6�A   c e r t i f i c a t e  s   b a s i c   c o n s t r a i n t   e x t e n s i o n   i s   n o t   m e t .   6�T h e   s t r i n g   i n c l u d e s   c h a r a c t e r s   t h a t   c a n n o t   b e   p r i n t e d .   6�C a n n o t   s e n d   m e s s a g e   % d   o v e r   W N S .   E r r o r   d e s c r i p t i o n :   % s   6�T h e   r e c i p i e n t   i s   n o t   t r u s t e d   f o r   t h e   s p e c i f i e d   a c t i o n .   6�C r y p t o g r a p h y   m o d u l e   i n t e r f a c e   e r r o r .   O b j e c t   n o t   f o u n d .   6�C a n n o t   w r i t e   t o   a   s t r e a m   t h a t   d o e s   n o t   s u p p o r t   w r i t i n g   7�I n t e r f a c e   e r r o r   i n   c r y p t o g r a p h y   m o d u l e .   I n v a l i d   l e n g t h .   7�E r r o r   g e t t i n g   c r y p t o g r a p h y   m o d u l e   i n f o r m a t i o n   ( 0 x % 0 8 X ) .   7�C r y p t o g r a p h y   m o d u l e   i n t e r f a c e   e r r o r .   I n v a l i d   p a r a m e t e r .   7�C a n n o t   r e a d   f r o m   a   s t r e a m   t h a t   d o e s   n o t   s u p p o r t   r e a d i n g   7�C r y p t o g r a p h y   m o d u l e   i n t e r f a c e   e r r o r .   I n v a l i d   s i g n a t u r e .   8�C r y p t o g r a p h y   m o d u l e   i n t e r f a c e   e r r o r .   I n v a l i d   p u b l i c   k e y .   8�E r r o r   r e m o v i n g   t h e   c e r t i f i c a t e   f r o m   r e p o s i t o r y   ( 0 x % 0 8 X ) .   8�Y o u   n e e d   a d m i n i s t r a t i v e   r i g h t s   t o   p e r f o r m   t h e   o p e r a t i o n .   9�T o o   m a n y   a r g u m e n t s   i n   s y s t e m   c o m m a n d   c a l l   ( d e p e n d s   o n   O S )   9�P e r s o n a l   c e r t i f i c a t e   r e p o s i t o r y   c o n t a i n s   n o   c e r t i f i c a t e s .   9�C r y p t o g r a p h y   m o d u l e   i n t e r f a c e   e r r o r .   I n s u f f i c i e n t   b u f f e r .   9�E r r o r   i m p o r t i n g   t h e   c e r t i f i c a t e   i n t o   r e p o s i t o r y   ( 0 x % 0 8 X ) .   :�D i s a l l o w e d   m a i l b o x   n a m e   s p e c i f i e d   ( S M T P   e r r o r   c o d e   _ c o d e _ )   :�C e r t i f i c a t e   C N   f i e l d   d o e s   n o t   m a t c h   t h e   t r a n s f e r r e d   v a l u e .   :�C r y p t o g r a p h y   m o d u l e   d o e s   n o t   s u p p o r t   s i g n a t u r e   a l g o r i t h m s .   :�T h e   r e q u e s t   t o   t h e   s e r v i c e   c o n t a i n s   a   m i s m a t c h e d   s e n d e r   I D   ;�C r y p t o g r a p h y   m o d u l e   i n t e r f a c e   e r r o r .   K e y s e t   i s   n o t   d e f i n e d .   ;�T h e   s e r v e r   r e q u i r e s   a u t h e n t i c a t i o n   ( S M T P   e r r o r   c o d e   _ c o d e _ )   ;�C e r t i f i c a t e   l a c k s   t h e   p r o p e r t y   t h a t   l i n k s   t o   a   p r i v a c y   k e y .   ;�N o   e x p e c t e d   a u t h e n t i c a t i o n   a t t r i b u t e   i n   c r y p t o g r a p h i c   d a t a .   ;�S y s t e m   h a s   i n s u f f i c i e n t   d i s k   s p a c e   ( S M T P   e r r o r   c o d e   _ c o d e _ )   ;�T h e   r e q u e s t e d   b u f f e r   s i z e   e x c e e d s   t h e   m a x i m u m   a l l o w e d   s i z e .   ;�C r y p t o g r a p h y   m o d u l e   d o e s   n o t   s u p p o r t   e n c r y p t i o n   a l g o r i t h m s .   ;�D e s c r i p t i o n   l a n g u a g e   o f   a b s t r a c t   d a t a   s y n t a x .   C o r r u p t   d a t a .   <�C r y p t o g r a p h i c   o p e r a t i o n s   p r o h i b i t e d   b y   t h e   s e c u r i t y   p r o f i l e .   <�A u t h e n t i c a t i o n   e r r o r .   C a n n o t   f i n d   a   s u p p o r t e d   s e c u r e   m e t h o d .   <�C r y p t o g r a p h y   m o d u l e   i n t e r f a c e   e r r o r .   N o   m o r e   a v a i l a b l e   d a t a .   <�C r y p t o g r a p h y   m o d u l e   i n t e r f a c e   e r r o r .   I n v a l i d   t y p e   s p e c i f i e d .   <�C e r t i f i c a t e   i n c l u d e s   u n k n o w n   e x t e n s i o n   m a r k e d   a s   " c r i t i c a l " .   <�C r y p t o g r a p h y   m o d u l e   i n t e r f a c e   e r r o r .   D a t a   a l r e a d y   e n c r y p t e d .   <�D e s c r i p t i o n   l a n g u a g e   o f   a b s t r a c t   d a t a   s y n t a x .   I n v a l i d   v a l u e .   =�C o m m a n d   p a r a m e t e r   i s   n o t   i m p l e m e n t e d   ( S M T P   e r r o r   c o d e   _ c o d e _ )   =�T h e   s u b j e c t   i s   n o t   f o u n d   i n   t h e   l i s t   o f   t r u s t e d   c e r t i f i c a t e s .   =�D e s c r i p t i o n   l a n g u a g e   o f   a b s t r a c t   d a t a   s y n t a x .   V a l u e   t o o   h i g h .   =�C r y p t o g r a p h y   m o d u l e   i n t e r f a c e   e r r o r .   I m p r o p e r   b u f f e r   o v e r l a p .   >�D e s c r i p t i o n   l a n g u a g e   o f   a b s t r a c t   d a t a   s y n t a x .   B u f f e r   o v e r f l o w .   >�C r y p t o g r a p h i c   d a t a   d o   n o t   i n c l u d e   a l l   t h e   r e q u i r e d   a t t r i b u t e s .   >�C r y p t o g r a p h y   m o d u l e   i n t e r f a c e   e r r o r .   I n v a l i d   K e y s e t   p a r a m e t e r .   ?�E n d   o n l y   c e r t i f i c a t e   u s e d   a s   v e r i f i c a t i o n   c e n t e r   o r   v i c e   v e r s a .   ?�C r y p t o g r a p h y   m o d u l e   i n t e r f a c e   e r r o r .   T h e   o b j e c t   a l r e a d y   e x i s t s .   ?�D e s c r i p t i o n   l a n g u a g e   o f   a b s t r a c t   d a t a   s y n t a x .   W r o n g   c h u n k   t y p e .   ?�C r y p t o g r a p h y   m o d u l e   i n t e r f a c e   e r r o r .   U s e r   p r o f i l e   i s   t e m p o r a r y .   @�S e r v i c e   d i s c o n n e c t e d   w i t h   c h a n n e l   b r e a k   ( S M T P   e r r o r   c o d e   _ c o d e _ )   @�N o t   e n o u g h   m e m o r y   f o r   c r y p t o g r a p h i c   d a t a   t o   c o m p l e t e   d e c r y p t i n g .   @�D e s c r i p t i o n   l a n g u a g e   o f   a b s t r a c t   d a t a   s y n t a x .   D a t a   e n d   e x p e c t e d .   @�E r r o r   a t t e m p t i n g   t o   c o n n e c t   t o   s e r v e r   " _ s e r v e r _ : _ p o r t _ "   ( _ c o d e _ )   @�U n a b l e   t o   l o c a t e   t h e   c e r t i f i c a t e   a n d   p r i v a c y   k e y   f o r   d e c r y p t i n g .   A�C r y p t o g r a p h y   m o d u l e   i n t e r f a c e   e r r o r .   I n v a l i d   a l g o r i t h m   s p e c i f i e d .   A�T e m p o r a r y   s e r v e r   a u t h e n t i c a t i o n   p r o b l e m s   ( S M T P   e r r o r   c o d e   _ c o d e _ )   A�C r y p t o g r a p h y   m o d u l e   i n t e r f a c e   e r r o r .   I n v a l i d   c r y p t o g r a p h y   m o d u l e .   A�U n a b l e   t o   e x e c u t e   t h e   o p e r a t i o n ,   I M A P   c o n n e c t i o n   n o t   e s t a b l i s h e d .   B�D e s c r i p t i o n   l a n g u a g e   o f   a b s t r a c t   d a t a   s y n t a x .   U n e x p e c t e d   d a t a   e n d .   B�D e s c r i p t i o n   l a n g u a g e   o f   a b s t r a c t   d a t a   s y n t a x .   N o t   y e t   i m p l e m e n t e d .   B�
 C e r t i f i c a t e   c o n n e c t e d   t o   c r y p t o g r a p h y   m o d u l e   " % s "   o f   t h e   t y p e   % d .   B�D e s c r i p t i o n   l a n g u a g e   o f   a b s t r a c t   d a t a   s y n t a x .   I n s u f f i c i e n t   m e m o r y .   B�D e s c r i p t i o n   l a n g u a g e   o f   a b s t r a c t   d a t a   s y n t a x .   W r o n g   e n c o d i n g   r u l e .   C�C a n n o t   p r o c e e d   w i t h   r e v o k i n g      u n a b l e   t o   v e r i f y   t h e   c e r t i f i c a t e ( s ) .   C�T h e   r e c i p i e n t   i s   n o t   a   v a l i d   d e l i v e r a b l e   n o t i f i c a t i o n   s u b s c r i b e r   I D   D�U n a b l e   t o   c h e c k   t h e   c e r t i f i c a t e   i n   t h e   l i s t   o f   r e v o k e d   c e r t i f i c a t e s .   D�T h e   s t r i n g   i n c l u d e s   c h a r a c t e r s   t h a t   a r e   n o t   p a r t   o f   t h e   7 - b i t   A S C I I .   D�D e s c r i p t i o n   l a n g u a g e   o f   a b s t r a c t   d a t a   s y n t a x .   R e s t r i c t i o n   v i o l a t i o n .   D�U n a b l e   t o   b u i l d   c e r t i f i c a t e   c h a i n   p r i o r   t o   t r u s t e d   r o o t   c e r t i f i c a t e .   E�T h e   e m a i l   s e r v e r   d o e s   n o t   s u p p o r t   t h e   O A u t h   2   a u t h e n t i c a t i o n   p r o t o c o l   E�C e r t i f i c a t e   n o t   f o u n d   i n   t h e   d a t a b a s e   o f   r e v o k e d   c e r t i f i c a t e s   s e r v e r .   F�T h e   c e r t i f i c a t e   h a s   b e e n   e x p l i c i t l y   m a r k e d   a s   n o t   t r u s t e d   b y   t h e   u s e r .   F�D e s c r i p t i o n   l a n g u a g e   o f   a b s t r a c t   d a t a   s y n t a x .   I n v a l i d   s e l e c t i o n   v a l u e .   G�C r y p t o g r a p h y   m o d u l e   i n t e r f a c e   e r r o r .   I n t e r n a l   c o n s i s t e n c y   c h e c k   f a i l e d .   G�T h e   c e r t i f i c a t e   d o e s   n o t   i n c l u d e   f i n a n c i a l   e x t e n s i o n s   A u t h e n t i c o d e ( t m ) .   G�I n v a l i d   c e r t i f i c a t e   n a m e .   T h e   n a m e   i s   n o t   i n   t h e   w h i t e   l i s t   o r   r e m o v e d .   G�U n a b l e   t o   l o c a t e   t h e   c e r t i f i c a t e   a n d   p r i v a c y   k e y   t o   u s e   f o r   d e c r y p t i o n .   H�D e s c r i p t i o n   l a n g u a g e   o f   a b s t r a c t   d a t a   s y n t a x .   U n k n o w n   e x t e n s i o n   o m i t t e d .   I�D e s c r i p t i o n   l a n g u a g e   o f   a b s t r a c t   d a t a   s y n t a x .   W r o n g   U n i c o d e   t e x t   ( U T F - 8 ) .   I�C r y p t o g r a p h y   m o d u l e   i n t e r f a c e   e r r o r .   I n v a l i d   c r y p t o g r a p h y   m o d u l e   v e r s i o n .   I�C r y p t o g r a p h y   m o d u l e   i n t e r f a c e   e r r o r .   T h e   k e y s e t   i s   r e g i s t e r e d   a s   i n v a l i d .   I�C r y p t o g r a p h y   m o d u l e   i n t e r f a c e   e r r o r .   D i g i t a l   s i g n a t u r e   f i l e   i s   c o r r u p t e d .   I�T o   c l o s e   t h e   f i n a l   o n e ,   t h e   a d d i t i o n a l   o n e s   s h o u l d   b e   r e l e a s e d   o r   c l o s e d .   J�C r y p t o g r a p h y   m o d u l e   i n t e r f a c e   e r r o r .   T h e   r e q u i r e d   a c t i o n   i s   n o t   s u p p o r t e d .   J�N o   d y n a m i c   l i n k   l i b r a r y   o r   e x p o r t e d   f u n c t i o n   l o c a t e d   t o   c h e c k   t h e   s u b j e c t .   J�C r y p t o g r a p h y   m o d u l e   i n t e r f a c e   e r r o r .   C r y p t o g r a p h y   m o d u l e   t y p e   n o t   d e f i n e d .   K�T h e   f i e l d   l e n g t h   m u s t   b e   g r e a t e r   t h a n   t h e   s p e c i f i e d   p r e c i s i o n   o r   e q u a l   t o   0   K�C r y p t o g r a p h y   m o d u l e   i n t e r f a c e   e r r o r .   U n a b l e   t o   d e c i p h e r   t h e   s p e c i f i e d   d a t a .   K�C r y p t o g r a p h y   m o d u l e   i n t e r f a c e   e r r o r .   T h i s   a c t i o n   r e q u i r e s   u s e r   i n t e r a c t i o n .   K�C r y p t o g r a p h y   m o d u l e   i n t e r f a c e   e r r o r .   I n s u f f i c i e n t   m e m o r y   f o r   t h e   o p e r a t i o n .   K�S i g n a t u r e   a n d / o r   c e r t i f i c a t e   t i m e s t a m p   c o u l d   n o t   b e   v e r i f i e d   o r   i s   i n v a l i d .   L�R e m o v e d   p r e v i o u s   c o n t e x t   o f   t h e   c e r t i f i c a t e   o r   l i s t   o f   r e v o k e d   c e r t i f i c a t e s .   M�I m p o r t a n t   f i e l d   ( i . e .   s u b j e c t   o r   i s s u e r )   m i s s i n g   o r   e m p t y   i n   t h e   c e r t i f i c a t e .   M�U n a b l e   t o   e x e c u t e   t h e   c r y p t o g r a p h i c   o p e r a t i o n   d u e   t o   l o c a l   p o l i t i c s   s e t t i n g s .   N�C r y p t o g r a p h y   m o d u l e   i n t e r f a c e   e r r o r .   C r y p t o g r a p h y   m o d u l e   i n i t i a l i z a t i o n   e r r o r .   O�P e r s o n a l   c e r t i f i c a t e   n o t   s p e c i f i e d   ( r e p o s i t o r y   c o n t a i n s   m u l t i p l e   c e r t i f i c a t e s ) .   O�C e r t i f i c a t e   c h a i n   p r o c e s s e d   b u t   o n e   o f   t h e   v e r i f i c a t i o n   c e n t e r s   i s   n o t   t r u s t e d .   O�N o n e   o f   t h e   c r y p t o g r a p h i c   d a t a   o r   t r u s t e d   c e r t i f i c a t e s   l i s t   s i g n e r s   i s   t r u s t e d .   O�U s e r   m a i l b o x   " _ u s e r _ "   o n   s e r v e r   " _ s e r v e r _ "   n o t   f o u n d . S e r v e r   r e s p o n s e :   " _ r e p l y _ "   P�C e r t i f i c a t e   u s e   d o e s   n o t   m a t c h   t h e   p u r p o s e   s p e c i f i e d   b y   i t s   v e r i f i c a t i o n   c e n t e r .   P�C r y p t o g r a p h y   m o d u l e   i n t e r f a c e   e r r o r .   I n v a l i d   c r y p t o g r a p h y   m o d u l e   t y p e   s p e c i f i e d .   Q�D a t a   r e q u i r e d   f o r   a u t h o r i z a t i o n   i n   t h e   p u s h   n o t i f i c a t i o n   s e r v i c e   i s   n o t   p r o v i d e d .   Q�C r y p t o g r a p h y   m o d u l e   d o e s   n o t   s u p p o r t   i n t r o d u c t i o n   o f   a   p a s s w o r d   t o   a   p r i v a c y   k e y .   Q�C a n n o t   p e r f o r m   t h e   o p e r a t i o n   b e c a u s e   t h e r e   i s   n o   c o n n e c t i o n   w i t h   t h e   S M T P   s e r v e r .   Q�C r y p t o g r a p h y   m o d u l e   i n t e r f a c e   e r r o r .   I m p r o p e r   k e y   f o r   u s e   i n   t h e   s p e c i f i e d   s t a t e .   Q�D e s c r i p t i o n   l a n g u a g e   o f   a b s t r a c t   d a t a   s y n t a x .   C e r t i f i c a t e   e n c o d e   e r r o r   c o d e   b a s e .   R�D e s c r i p t i o n   l a n g u a g e   o f   a b s t r a c t   d a t a   s y n t a x .   I n v a l i d   a r g u m e n t s   f o r   f u n c t i o n   c a l l .   R�C r y p t o g r a p h y   m o d u l e   i n t e r f a c e   e r r o r .   I m p r o p e r   h a s h   f o r   u s e   i n   t h e   s p e c i f i e d   s t a t e .   R�C r y p t o g r a p h y   m o d u l e   i n t e r f a c e   e r r o r .   T h e   c r y p t o g r a p h y   m o d u l e   l i b r a r y   i s   n o t   f o u n d .   T�C o m m a n d   s y n t a x   e r r o r .   P o s s i b l e   e r r o r   e n t e r i n g   e m a i l   a d d r e s s   ( S M T P   e r r o r   c o d e   _ c o d e _ )   U�D e s c r i p t i o n   l a n g u a g e   o f   a b s t r a c t   d a t a   s y n t a x .   I n t e r n a l   e r r o r   o f   c e r t i f i c a t e   e n c o d i n g .   U�T h i s   o p e r a t i o n   c a n n o t   b e   p e r f o r m e d   b e c a u s e   n o   P O P 3   s e r v e r   a d d r e s s   h a s   b e e n   s p e c i f i e d .   W�D a t a   t r a n s f e r   e r r o r .   T h e   n o t i f i c a t i o n   d e l i v e r y   s e r v i c e   c a n n o t   p r o c e s s   t h e   n o t i f i c a t i o n s   X�T h e   n o t i f i c a t i o n   r e c i p i e n t   i s   a   m o b i l e   a p p l i c a t i o n   t h a t   i s   n o t   r e g i s t e r e d   i n   t h e   s e r v i c e   Z�U n a b l e   t o   v e r i f y   i f   u s a g e   b y   t h e   s u b j e c t   i s   p o s s i b l e :   t h e   c o r r e s p o n d i n g   s e r v e r   i s   o f f l i n e .   [�C r y p t o g r a p h y   m o d u l e   i n t e r f a c e   e r r o r .   T h e   c r y p t o g r a p h y   m o d u l e   t y p e   i s   r e g i s t e r e d   a s   i n v a l i d .   \�T h e   r e q u i r e d   a u t h e n t i c a t i o n   a c t i o n   i s   n o t   s u p p o r t e d   b y   t h e   s e l e c t e d   c e r t i f i c a t i o n   a u t h o r i t y .   ^�T h e   f i l e   i s   o p e n e d   w i t h   i n c o r r e c t   p a r a m e t e r s :   t o   c r e a t e   a   f i l e ,   y o u   n e e d   t h e   w r i t e   p e r m i s s i o n .   `�D e s c r i p t i o n   l a n g u a g e   o f   a b s t r a c t   d a t a   s y n t a x .   T h e   f u n c t i o n   i s   n o t   s u p p o r t e d   f o r   t h i s   c h u n k   t y p e .   `�E r r o r   r e m o v i n g   t h e   c e r t i f i c a t e   f r o m   r e p o s i t o r y :   r e m o v e d   c e r t i f i c a t e   n o t   f o u n d   i n   t h e   r e p o s i t o r y .   b�T h e   f i l e   i s   o p e n e d   w i t h   i n c o r r e c t   p a r a m e t e r s :   t o   w r i t e   t o   t h e   f i l e ,   y o u   n e e d   t h e   w r i t e   p e r m i s s i o n .   d�C e r t i f i c a t e   c h a i n   p r o c e s s e d   b u t   i n t e r r u p t e d   o n   a   r o o t   c e r t i f i c a t e   t h a t   i s   n o t   a   t r u s t e d   c e r t i f i c a t e .   e�U n a b l e   t o   c o n t i n u e   P u t   o p e r a t i o n :   f i l e   s h o u l d   b e   r e s i z e d   a n d   s i g n a t u r e   i n s e r t i o n   s h o u l d   b e   c o m p l e t e d .   e�C r y p t o g r a p h y   m o d u l e   i n t e r f a c e   e r r o r .   T h e   c r y p t o g r a p h y   m o d u l e   t y p e   d o e s   n o t   m a t c h   t h e   s p e c i f i e d   v a l u e .   g�C h a i n   e n d s   w i t h   a   t e s t   r o o t   c e r t i f i c a t e   t h a t   i s   n o t   a   t r u s t e d   c e r t i f i c a t e   w i t h   c u r r e n t   p o l i c y   s e t t i n g s .   i�U n a b l e   t o   c h e c k   t h e   c e r t i f i c a t e   i n   t h e   l i s t   o f   r e v o k e d   c e r t i f i c a t e s :   t h e   c o r r e s p o n d i n g   s e r v e r   i s   o f f l i n e .   k�T h e   c e r t i f i c a t e   a s s o c i a t e d   w i t h   t h e   p r i v a c y   k e y   p o i n t s   t o   a   c r y p t o g r a p h y   m o d u l e   o t h e r   t h a n   t h e   c u r r e n t   o n e .   m�T h e   s t r i n g   i n c l u d e s   a   k e y ,   o b j e c t   i d e n t i f i e r   o r   s e p a r a t o r   t h a t   a r e   n o t   v a l i d   f o r   t h e   n a m e   a t t r i b u t e   i n   X . 5 0 0 .   n�T h e   f o r m   s p e c i f i e d   f o r   t h e   o b j e c t   i s   e i t h e r   n o t   s u p p o r t e d   o r   u n k n o w n   f o r   t h e   s e l e c t e d   c e r t i f i c a t i o n   a u t h o r i t y .   r�T h e   p u s h   n o t i f i c a t i o n   c h a n n e l   i s   n o t   v a l i d   o r   n o t   r e c o g n i z e d .   R e q u e s t   a   n e w   d e l i v e r a b l e   n o t i f i c a t i o n   s u b s c r i b e r   I D   v�C r y p t o g r a p h y   m o d u l e   i n t e r f a c e   e r r o r .   T h e   c r y p t o g r a p h y   m o d u l e   d o e s   n o t   s u p p o r t   t h e   h a s h   c o d e   u s e d   t o   i d e n t i f y   m e s s a g e s .   w�N o   d y n a m i c   l i n k   l i b r a r y   o r   e x p o r t e d   f u n c t i o n   l o c a t e d   t o   c h e c k   t h e   c e r t i f i c a t e   a g a i n s t   t h e   l i s t   o f   r e v o k e d   c e r t i f i c a t e s .   z�C a n n o t   d e t e r m i n e   t h e   c e r t i f i c a t e   t y p e .   P o s s i b l y   t h e   c e r t i f i c a t e   i s   n o t   i s s u e d   f o r   A P N s   o r   i t   d o e s   n o t   h a v e   t h e   P E M   f o r m a t .   z�C r y p t o g r a p h y   m o d u l e   i n t e r f a c e   e r r o r .   T h e   s e c u r i t y   t o k e n   d o e s   n o t   h a v e   e n o u g h   a v a i l a b l e   m e m o r y   f o r   a n   a d d i t i o n a l   c o n t a i n e r .   }�T h e   r e q u i r e d   c e r t i f i c a t e   f a i l e d   v a l i d i t y   p e r i o d   v e r i f i c a t i o n   u p o n   c o m p a r i s o n   w i t h   s y s t e m   c l o c k   o r   s i g n a t u r e   t i m e   i n   t h e   f i l e .   ��T h e   p o s i t i o n   i n   t h e   u n d e r l y i n g   s t r e a m   w a s   c h a n g e d   o u t s i d e   o f   t h i s   D a t a W r i t e r   o b j e c t .   T h e   a p p l i c a t i o n   m i g h t   f u n c t i o n   i n c o r r e c t l y .   ��T h e   p o s i t i o n   i n   t h e   u n d e r l y i n g   s t r e a m   w a s   c h a n g e d   o u t s i d e   o f   t h i s   D a t a R e a d e r   o b j e c t .   T h e   a p p l i c a t i o n   m i g h t   f u n c t i o n   i n c o r r e c t l y .   ��C r y p t o g r a p h y   m o d u l e   i n t e r f a c e   e r r o r .   T h e   m a i n   p a r a m e t e r s   c o u l d   n o t   b e   s e t   b e c a u s e   t h e   c r y p t o g r a p h y   m o d u l e   u s e s   f i x e d   p a r a m e t e r s .   ��C a n n o t   v e r i f y   t h e   s i g n a t u r e .   P o s s i b l e   r e a s o n :   t h e   s i g n a t u r e   a n d   t h e   l o c a l   c e r t i f i c a t e   s t o r e   d o   n o t   c o n t a i n   t h e   s i g n e r   c e r t i f i c a t e .   ��C r y p t o g r a p h y   m o d u l e   i n t e r f a c e   e r r o r .   T h e   c r y p t o g r a p h y   m o d u l e   i s   u n a b l e   t o   e x e c u t e   t h e   r e q u i r e d   a c t i o n :   t h e   c o n t e x t   w a s   o b t a i n e d   i n   a   l i m i t e d   m o d e .   ��O b j e c t s   n o t   a v a i l a b l e   f o r   I n t e r n e t   a c c e s s .   W o r k i n g   w i t h   t h e s e   o b j e c t s   o n   a   c o m p u t e r   r e q u i r e s   i n s t a l l a t i o n   o f   I n t e r n e t   E x p l o r e r   v e r s i o n   5 . 0   o r   h i g h e r .   ��D e s c r i p t i o n   l a n g u a g e   o f   a b s t r a c t   d a t a   s y n t a x .   I n v a l i d   t a g   v a l u e   i d e n t i f i e d .   ( A   c e r t i f i c a t e   t h a t   i s   n o t   s u p p o r t e d   b y   t h e   c r y p t o g r a p h y   m o d u l e   c o u l d   b e   s p e c i f i e d ) .   ��T h e   p r i v a t e   k e y   c o n t a i n e r   i s   a v a i l a b l e   b u t   a n   o p e r a t i o n   o n   t h i s   c o n t a i n e r   r e t u r n e d   a n   e r r o r . 
 U s u a l l y ,   t h i s   i s   d u e   t o   i n c o r r e c t   a c c e s s   p a s s w o r d   t o   a   p r i v a t e   k e y . 
   �  �PNG

   IHDR   p      !�{�  �IDATH�c���?-���4��hr�+F�Y��A���Y�h�I�H	�M���K���'V�Ѵ
c�+	t@��욞�M�Zb��5�6++1������%�%��y0L��'A����{������-R#�?��
0b�Zb��5�֘�|�g��A���`�w�l����z�ɉ@�"��P�}��_L�Zb��5���-2*����BE�de����Aw'�����S���/"m�ګlz��I�Zb��5��H��D@���S�s[�i��!P��0�u�J����)�@J�.K������덴�ӵs�(�������^��%V�\��A D��w��<�@�9j
�Ax��iu %x����*<�A49j�#�,BظT� (M��A�x�ڍ��J<��ΐ�>6CA�?z��I��FW ��k<��    IEND�B`����k  �PNG

   IHDR   p      O��   cPLTE� ��@ 㠐�nT�ZO�=�iS�. L�� 1v�9Cw�洣	|	ݝ��(݃u�7�8� =��3���x�o�x�w��̞��|�ǋP�Jr�Ã�~�   tRNS @��f   �IDAT(ϥQ�� 㰋�������J�	9�(1�MO��!>��
$Z����D
G�$Z����KǙ&ahN����	����@���NB &��$ZZ_-T�RIY���^~K&�"Vm��|p�7�'���h��_}zL����#H&��ShT�H�ikC����R�dB��?d<>�Һ>�    IEND�B`��  ﻿{
  { "sıfır", "bir", "İki", "üç", "dörd", "beş", "altı", "yeddi", "səkkiz", "doqquz" },
  { "on", "İyirmi", "otuz", "qırx", "əlli", "altmış", "yetmiş", "səksən", "doxsan" },
  { "yüz" },
  { "min" },
  { "milyon" },
  { "milyard" },
  { "trilyon" }
}����9  ﻿{
  { "ноль" }, 
  { "один", "одна", "одно" }, 
  { "два", "две" },
  { "три", "четыре", "пять", "шесть", "семь", "восемь", "девять" },
  { "одиннадцать", "двенадцать", "тринадцать", "четырнадцать", "пятнадцать", "шестнадцать", "семнадцать", "восемнадцать", "девятнадцать" },
  { "десять", "двадцать", "тридцать", "сорок", "пятьдесят", "шестьдесят", "семьдесят", "восемьдесят", "девяносто" },
  { "сто", "двести", "триста", "четыреста", "пятьсот", "шестьсот", "семьсот", "восемьсот", "девятьсот" },
  { "тысяча", "тысячи", "тысяч", 1 },
  { "миллион", "миллиона", "миллионов", 0 },
  { "миллиард", "миллиарда", "миллиардов", 0 },
  { "триллион", "триллиона", "триллионов", 0 }
}���  ﻿{
  { "и" },
  { "нула" }, 
  { "един", "една", "едно", "едни" }, 
  { "два", "две" },
  { "три", "четири", "пет", "шест", "седем", "осем", "девет" },
  { "единадесет", "дванадесет", "тринадесет", "четиринадесет", "петнадесет", "шестнадесет", "седемнадесет", "осемнадесет", "деветнадесет" },
  { "десет", "двадесет", "тридесет", "четиридесет", "петдесет", "шестдесет", "седемдесет", "осемдесет", "деветдесет" },
  { "сто", "двеста", "триста", "четиристотин", "петстотин", "шестстотин", "седемстотин", "осемстотин", "деветстотин" },
  { "хиляда", "хиляди", 1 },
  { "милион", "милиона", 0 },
  { "милиард", "милиарда", 0 },
  { "трилион", "трилиона", 0 },
}�������������u  ﻿{
  { "und" }, 
  { "null" }, 
  { "ein", "eine", "ein" }, 
  { "zwei", "drei", "vier", "fünf", "sechs", "sieben", "acht", "neun" },
  { "elf", "zwölf", "dreizehn", "vierzehn", "fünfzehn", "sechzehn", "siebzehn", "achtzehn", "neunzehn" },
  { "zehn", "zwanzig", "dreißig", "vierzig", "fünfzig", "sechzig", "siebzig", "achtzig", "neunzig" },
  { "einhundert", "zweihundert", "dreihundert", "vierhundert", "fünfhundert", "sechshundert", "siebenhundert", "achthundert", "neunhundert" },
  { "tausend", 0 },
  { "Million", "Millionen", 1 },
  { "Milliarde", "Milliarden", 1 },
  { "Trillion", "Trillionen", 1 }
}��������  ﻿{
  { "zero" , "one", "two", "three", "four", "five", "six", "seven", "eight", "nine" },
  { "eleven","twelve","thirteen","fourteen","fifteen","sixteen","seventeen","eighteen","nineteen" },
  { "ten", "twenty","thirty","forty","fifty","sixty","seventy","eighty","ninety" },
  { "hundred", "hundred" },
  { "thousand", "thousand" },
  { "million", "million" },
  { "billion", "billion" },
  { "trillion", "trillion" }
}��������������r  ﻿{
  { "null" , "üks", "kaks", "kolm", "neli", "viis", "kuus", "seitse", "kaheksa", "üheksa" },
  { "üksteist", "kaksteist", "kolmteist", "neliteist", "viisteist", "kuusteist", "seitseteist", "kaheksateist", "üheksateist" },
  { "kümme", "kakskümmend", "kolmkümmend", "nelikümmend", "viiskümmend", "kuuskümmend", "seitsekümmend", "kaheksakümmend", "üheksakümmend" },
  { "ükssada", "kakssada", "kolmsada", "nelisada", "viissada", "kuussada", "seitsesada", "kaheksasada", "üheksasada" },
  { "tuhat", "tuhat" },
  { "miljon", "miljonit" },
  { "miljard", "miljardit" },
  { "triljon", "triljonit" }
}����������  <?xml version="1.0" encoding="utf-8"?>
<xs:schema xmlns:xs="http://www.w3.org/2001/XMLSchema"
        xmlns:tns="http://v8.1c.ru/8.3/data/ext"
        xmlns:core="http://v8.1c.ru/8.1/data/core"
        targetNamespace="http://v8.1c.ru/8.3/data/ext"
        elementFormDefault="qualified"
        attributeFormDefault="unqualified">

  <xs:import namespace="http://v8.1c.ru/8.1/data/core" schemaLocation="../../../xdto/src/res/core.xsd"/>

  <xs:complexType name="GeographicCoordinates">
        <xs:annotation>
            <xs:documentation>Географические координаты</xs:documentation>
        </xs:annotation>
      <xs:attribute name="latitude" type="xs:double" use="required"/>
      <xs:attribute name="longitude" type="xs:double" use="required"/>
      <xs:attribute name="altitude" type="xs:double" default="0"/>
    </xs:complexType>

    <xs:complexType name="LocationData">
        <xs:annotation>
            <xs:documentation> Данные местоположения</xs:documentation>
        </xs:annotation>
        <xs:sequence>
            <!-- Координаты. -->
            <xs:element name="coordinates" type="tns:GeographicCoordinates" />
            <!-- Направление движения. -->
            <xs:element name="heading" type="xs:double" />
            <!-- Скорость движения. -->
            <xs:element name="speed" type="xs:double" />
            <!-- Горизонтальная точность. -->
            <xs:element name="horizontalAccuracy" type="xs:double" />
            <!-- Вертикальная точность. -->
            <xs:element name="verticalAccurcy" type="xs:double" />
        </xs:sequence>
        <xs:attribute name="date" type="xs:dateTime" />
    </xs:complexType>
    
    <xs:simpleType name="DeliverableNotificationSubscriberType">
        <!-- Тип подписчика уведомлений -->
        <xs:restriction base="xs:string">
            <xs:enumeration value="APNS" />
            <xs:enumeration value="GCM" />
            <xs:enumeration value="WNS" />
            <xs:enumeration value="FCM" />
        </xs:restriction>
    </xs:simpleType>
    
    <xs:simpleType name="SoundAlert">
        <!-- ЗвуковоеОповещение -->
        <xs:restriction base="xs:string">
            <xs:enumeration value="None" />
            <xs:enumeration value="Default" />
        </xs:restriction>
    </xs:simpleType>

    <xs:simpleType name="DeliverableNotificationSendErrorType">
        <!-- ТипОшибкиОтправкиДоставляемогоУведомления -->
        <xs:restriction base="xs:string">
            <xs:enumeration value="AuthenticationDataError" />
            <xs:enumeration value="NotificationBodyError" />
            <xs:enumeration value="NotificationsLimitExceeded" />
            <xs:enumeration value="DeliverableNotificationSubscriberIDError" />
            <xs:enumeration value="PushServiceProviderConnectionError" />
            <xs:enumeration value="PushServiceProviderError" />
            <xs:enumeration value="UnknownError" />
        </xs:restriction>
    </xs:simpleType>

    <xs:complexType name="DeliverableNotificationSubscriberID">
        <xs:annotation>
            <xs:documentation> Подписчик уведомлений</xs:documentation>
        </xs:annotation>
        <xs:sequence>
            <!-- Тип подписчика ios/android -->
            <xs:element name="subscriberType" type="tns:DeliverableNotificationSubscriberType" />
            <!-- идентификатор устройства-->
            <xs:element name="deviceID" type="xs:string" />
            <!-- идентификатор приложения-->
            <xs:element name="applicationID" type="xs:string" />
            <!-- идентификатор базы-->
            <xs:element name="databaseID" type="xs:string" />
        </xs:sequence>
    </xs:complexType>

    <xs:simpleType name="InAppPurchaseService">
        <xs:restriction base="xs:string">
            <xs:enumeration value="GooglePlayInAppBilling" />
            <xs:enumeration value="AppleInAppPurchase" />
            <xs:enumeration value="WindowsInAppPurchase" />
        </xs:restriction>
    </xs:simpleType>

    <xs:complexType name="InAppPurchaseReceipt">
        <xs:annotation>
            <xs:documentation> Квитанция О Встроенной Покупке</xs:documentation>
        </xs:annotation>
        <xs:sequence>
            <xs:element name="service" type="tns:InAppPurchaseService" minOccurs="0" />
            <xs:element name="data" type="xs:base64Binary" minOccurs="0" />
      </xs:sequence>
    </xs:complexType>


</xs:schema>
�����  ﻿{
  { "nolla" , "yksi", "kaksi", "kolme", "neljä", "viisi", "kuusi", "seitsemän", "kahdeksan", "yhdeksän" },
  { "yksitoista","kaksitoista","kolmetoista","neljätoista","viisitoista","kuusitoista","seitsemäntoista","kahdeksantoista","yhdeksäntoista" },
  { "kymmenen", "kaksikymmentä","kolmekymmentä","neljäkymmentä","viisikymmentä","kuusikymmentä","seitsemänkymmentä","kahdeksankymmentä","yhdeksänkymmentä" },
  { "sata", "kaksisataa", "kolmesataa", "neljäsataa", "viisisataa", "kuusisataa", "seitsemänsataa", "kahdeksansataa", "yhdeksänsataa" },
  { "tuhat", "tuhatta" },
  { "miljoona", "miljoonaa" },
  { "miljardi", "miljardia" },
  { "triljoona", "triljoonaa" }
}�  ﻿{
  {"zéro", "un",   "deux",  "trois",  "quatre",   "cinq",      "six", "sept", "huit", "neuf"},
  {        "onze", "douze", "treize", "quatorze", "quinze",    "seize"},
  {        "dix",  "vingt", "trente", "quarante", "cinquante", "soixante"},
  {"cent", "cents"},
  {"mille", "mille"},
  {"million", "millions"},
  {"milliard", "milliards"},
  {"trillion", "trillions"},
  {"et"}
}��������������  ﻿{
  { "nulla", "egy", "kettő", "három", "négy", "öt", "hat", "hét", "nyolc", "kilenc" },
  { "tíz", "húsz","harminc","negyven","ötven","hatvan","hetven","nyolcvan","kilencven" },
  { "tizen", "huszon" },
  { "száz", "ezer", "millió", "milliárd", "trillió" }
}�����(  ﻿{
  { "ი", "და" },
  { "ნული", "ერთი", "ორი", "სამი", "ოთხი", "ხუთი", "ექვსი", "შვიდი", "რვა", "ცხრა",
    "ათი", "თერთმეტი", "თორმეტი", "ცამეტი", "თოთხმეტი", "თხუთმეტი", "თექვსმეტი", "ჩვიდმეტი", "თვრამეტი", "ცხრამეტი" },
  { "ოც", "ორმოც", "სამოც", "ოთხმოც" },
  { "ას", "ორას", "სამას", "ოთხას", "ხუთას", "ექვსას", "შვიდას", "რვაას", "ცხრაას" },
  { "ათას", "მილიონ", "მილიარდ", "ტრილიონ" }
}����  ﻿{
  { "ноль", "бір", "екі", "үш", "төрт", "бес", "алты", "жеті", "сегіз", "тоғыз" },
  { "он", "жиырма", "отыз", "қырық", "елу", "алпыс", "жетпіс", "сексен", "тоқсан" },
  { "бір жүз", "екі жүз", "үш жүз", "төрт жүз", "бес жүз", "алты жүз", "жеті жүз", "сегіз жүз", "тоғыз жүз" },
  { "мың" },
  { "миллион" },
  { "миллиард" },
  { "триллион" }
}<  ﻿{
  { "nulis" }, 
  { "vienas", "viena" }, 
  { "du", "dvi" },
  { "trys", "trys" },
  { "keturi", "keturios" },
  { "penki", "penkios" },
  { "šeši", "šešios" },
  { "septyni", "septynios" },
  { "aštuoni", "aštuonios" },
  { "devyni", "devynios" },
  { "vienuolika", "dvylika", "trylika", "keturiolika", "penkiolika", 
    "šešiolika", "septyniolika", "aštuoniolika", "devyniolika" },
  { "dešimt", "dvidešimt", "trisdešimt", "keturiasdešimt", "penkiasdešimt",
    "šešiasdešimt", "septyniasdešimt", "aštuoniasdešimt", "devyniasdešimt" },
  { "šimtas", "šimtai", "", 0 },
  { "tūkstantis", "tūkstančiai", "tūkstančių", 0 },
  { "milijonas", "milijonai", "milijonų", 0 },
  { "milijardas", "milijardai", "milijardų", 0 },
  { "trilijonas", "trilijonai", "trilijonų", 0 }
}  ﻿{
	{ "nulle" },
	{ "viens", "viena" },
	{ "divi", "divas" },
	{ "trīs", "trijas" },
	{ "četri", "četras" },
	{ "pieci", "piecas" },
	{ "seši", "sešas" },
	{ "septiņi", "septiņas" },
	{ "astoņi", "astoņas" },
	{ "deviņi", "deviņas" },
	{ "vienpadsmit", "divpadsmit", "trīspadsmit", "četrpadsmit", "piecpadsmit", "sešpadsmit", "septiņpadsmit", "astoņpadsmit", "deviņpadsmit" },
	{ "desmit", "divdesmit", "trīsdesmit", "četrdesmit", "piecdesmit", "sešdesmit", "septiņdesmit", "astoņdesmit", "deviņdesmit" },
	{ "simts", "divi simti", "trīs simti",  "četri simti", "pieci simti", "seši simti", "septiņi simti", "astoņi simti", "deviņi simti" },
	{ "tūkstotis", "divi tūkstoši", "trīs tūkstoši", "četri tūkstoši", "pieci tūkstoši", "seši tūkstoši", "septiņi tūkstoši", "astoņii tūkstoši", "deviņi tūkstoši" , "desmit tūkstoši" },
	{ "simt tūkstoši" },
	{ "tūkstotis", "tūkstoši" },
	{ "miljons", "miljoni" },
	{ "miljards", "miljardi" },
	{ "trilions", "trilioni" }
}���������
  ﻿<model xmlns="http://v8.1c.ru/8.1/xdto" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
	<package targetNamespace="http://v8.1c.ru/8.3/data/ext" elementFormQualified="true" attributeFormQualified="false">
		<import namespace="http://v8.1c.ru/8.1/data/core"/>
		<valueType name="DeliverableNotificationSendErrorType" base="xs:string">
			<enumeration>AuthenticationDataError</enumeration>
			<enumeration>NotificationBodyError</enumeration>
			<enumeration>NotificationsLimitExceeded</enumeration>
			<enumeration>DeliverableNotificationSubscriberIDError</enumeration>
			<enumeration>PushServiceProviderConnectionError</enumeration>
			<enumeration>PushServiceProviderError</enumeration>
			<enumeration>UnknownError</enumeration>
		</valueType>
		<valueType name="DeliverableNotificationSubscriberType" base="xs:string">
			<enumeration>APNS</enumeration>
			<enumeration>GCM</enumeration>
			<enumeration>WNS</enumeration>
			<enumeration>FCM</enumeration>
		</valueType>
		<valueType name="InAppPurchaseService" base="xs:string">
			<enumeration>GooglePlayInAppBilling</enumeration>
			<enumeration>AppleInAppPurchase</enumeration>
			<enumeration>WindowsInAppPurchase</enumeration>
		</valueType>
		<valueType name="SoundAlert" base="xs:string">
			<enumeration>None</enumeration>
			<enumeration>Default</enumeration>
		</valueType>
		<objectType name="DeliverableNotificationSubscriberID">
			<property xmlns:d4p1="http://v8.1c.ru/8.3/data/ext" name="subscriberType" type="d4p1:DeliverableNotificationSubscriberType"/>
			<property name="deviceID" type="xs:string"/>
			<property name="applicationID" type="xs:string"/>
			<property name="databaseID" type="xs:string"/>
		</objectType>
		<objectType name="GeographicCoordinates">
			<property name="latitude" type="xs:double" form="Attribute"/>
			<property name="longitude" type="xs:double" form="Attribute"/>
			<property name="altitude" type="xs:double" lowerBound="0" default="0" form="Attribute"/>
		</objectType>
		<objectType name="InAppPurchaseReceipt">
			<property xmlns:d4p1="http://v8.1c.ru/8.3/data/ext" name="service" type="d4p1:InAppPurchaseService" lowerBound="0"/>
			<property name="data" type="xs:base64Binary" lowerBound="0"/>
		</objectType>
		<objectType name="LocationData">
			<property name="date" type="xs:dateTime" lowerBound="0" form="Attribute"/>
			<property xmlns:d4p1="http://v8.1c.ru/8.3/data/ext" name="coordinates" type="d4p1:GeographicCoordinates"/>
			<property name="heading" type="xs:double"/>
			<property name="speed" type="xs:double"/>
			<property name="horizontalAccuracy" type="xs:double"/>
			<property name="verticalAccurcy" type="xs:double"/>
		</objectType>
	</package>
</model>��������  ﻿{
  { "zero" }, 
  { "jeden", "jedna", "jedno" }, 
  { "dwa", "dwie" },
  { "trzy", "cztery", "pięć", "sześć", "siedem", "osiem", "dziewięć" },
  { "jedenaście", "dwanaście", "trzynaście", "czternaście", "piętnaście", "szesnaście", "siedemnaście", "osiemnaście", "dziewiętnaście" },
  { "dziesięć", "dwadzieścia", "trzydzieści", "czterdzieści", "pięćdziesiąt", "sześćdziesiąt", "siedemdziesiąt", "osiemdziesiąt", "dziewięćdziesiąt" },
  { "sto", "dwieście", "trzysta", "czterysta", "pięćset", "sześćset", "siedemset", "osiemset", "dziewięćset" },
  { "tysiąc", "tysiące", "tysięcy", 0 },
  { "milion", "miliony", "milionów", 0 },
  { "miliard", "miliardy", "miliardów", 0 },
  { "trylion", "tryliony", "trylionów", 0 }
}���������������  ﻿{
  { "zero" }, 
  { "jeden", "", "" }, 
  { "dwóch", "" },
  { "trzech", "czterech", "pięсiu", "sześciu", "siedmiu", "ośmiu", "dziewięciu" },
  { "jedenastu", "dwunastu", "trzynastu", "czternastu", "piętnastu", "szesnastu", "siedemnastu", "osiemnastu", "dziewiętnastu" },
  { "dziesięciu", "dwudziestu", "trzydziestu", "czterdziestu", "pięćdziesięciu", "sześćdziesięciu", "siedemdziesięciu", "osiemdziesięciu", "dziewięćdziesięciu" },
  { "stu", "dwustu", "trzystu", "czterystu", "pięciuset", "sześciuset", "siedmiuset", "ośmiuset", "dziewięciuset" },
  { "tysiąc", "tysiące", "tysięcy", 0 },
  { "milion", "miliony", "milionów", 0 },
  { "miliard", "miliardy", "miliardów", 0 },
  { "trylion", "tryliony", "trylionów", 0 }
}���������  ﻿// ***************************************************************************
// *
// * Copyright (C) 2015 International Business Machines
// * Corporation and others. All Rights Reserved.
// * Tool: org.unicode.cldr.icu.NewLdml2IcuConverter
// * Source File: <path>/plurals.xml, ordinals.xml
// *
// ***************************************************************************
plurals:table(nofallback){
    locales{
        af{"set8"}
        ak{"set5"}
        am{"set1"}
        ar{"set34"}
        as{"set1"}
        asa{"set8"}
        ast{"set3"}
        az{"set8"}
        be{"set27"}
        bem{"set8"}
        bez{"set8"}
        bg{"set8"}
        bh{"set5"}
        bm{"set0"}
        bn{"set1"}
        bo{"set0"}
        br{"set31"}
        brx{"set8"}
        bs{"set20"}
        ca{"set3"}
        ce{"set8"}
        cgg{"set8"}
        chr{"set8"}
        ckb{"set8"}
        cs{"set25"}
        cy{"set35"}
        da{"set10"}
        de{"set3"}
        dsb{"set23"}
        dv{"set8"}
        dz{"set0"}
        ee{"set8"}
        el{"set8"}
        en{"set3"}
        eo{"set8"}
        es{"set8"}
        et{"set3"}
        eu{"set8"}
        fa{"set1"}
        ff{"set2"}
        fi{"set3"}
        fil{"set13"}
        fo{"set8"}
        fr{"set2"}
        fur{"set8"}
        fy{"set3"}
        ga{"set32"}
        gd{"set21"}
        gl{"set3"}
        gsw{"set8"}
        gu{"set1"}
        guw{"set5"}
        gv{"set33"}
        ha{"set8"}
        haw{"set8"}
        he{"set24"}
        hi{"set1"}
        hr{"set20"}
        hsb{"set23"}
        hu{"set8"}
        hy{"set2"}
        id{"set0"}
        ig{"set0"}
        ii{"set0"}
        in{"set0"}
        is{"set11"}
        it{"set3"}
        iu{"set17"}
        iw{"set24"}
        ja{"set0"}
        jbo{"set0"}
        jgo{"set8"}
        ji{"set3"}
        jmc{"set8"}
        jv{"set0"}
        jw{"set0"}
        ka{"set8"}
        kab{"set2"}
        kaj{"set8"}
        kcg{"set8"}
        kde{"set0"}
        kea{"set0"}
        kk{"set8"}
        kkj{"set8"}
        kl{"set8"}
        km{"set0"}
        kn{"set1"}
        ko{"set0"}
        ks{"set8"}
        ksb{"set8"}
        ksh{"set16"}
        ku{"set8"}
        kw{"set17"}
        ky{"set8"}
        lag{"set15"}
        lb{"set8"}
        lg{"set8"}
        lkt{"set0"}
        ln{"set5"}
        lo{"set0"}
        lt{"set28"}
        lv{"set14"}
        mas{"set8"}
        mg{"set5"}
        mgo{"set8"}
        mk{"set12"}
        ml{"set8"}
        mn{"set8"}
        mo{"set19"}
        mr{"set1"}
        ms{"set0"}
        mt{"set29"}
        my{"set0"}
        nah{"set8"}
        naq{"set17"}
        nb{"set8"}
        nd{"set8"}
        ne{"set8"}
        nl{"set3"}
        nn{"set8"}
        nnh{"set8"}
        no{"set8"}
        nqo{"set0"}
        nr{"set8"}
        nso{"set5"}
        ny{"set8"}
        nyn{"set8"}
        om{"set8"}
        or{"set8"}
        os{"set8"}
        pa{"set5"}
        pap{"set8"}
        pl{"set26"}
        prg{"set14"}
        ps{"set8"}
        pt{"set7"}
        pt_PT{"set9"}
        rm{"set8"}
        ro{"set19"}
        rof{"set8"}
        root{"set0"}
        ru{"set30"}
        rwk{"set8"}
        sah{"set0"}
        saq{"set8"}
        se{"set17"}
        seh{"set8"}
        ses{"set0"}
        sg{"set0"}
        sh{"set20"}
        shi{"set18"}
        si{"set4"}
        sk{"set25"}
        sl{"set22"}
        sma{"set17"}
        smi{"set17"}
        smj{"set17"}
        smn{"set17"}
        sms{"set17"}
        sn{"set8"}
        so{"set8"}
        sq{"set8"}
        sr{"set20"}
        ss{"set8"}
        ssy{"set8"}
        st{"set8"}
        sv{"set3"}
        sw{"set3"}
        syr{"set8"}
        ta{"set8"}
        te{"set8"}
        teo{"set8"}
        th{"set0"}
        ti{"set5"}
        tig{"set8"}
        tk{"set8"}
        tl{"set13"}
        tn{"set8"}
        to{"set0"}
        tr{"set8"}
        ts{"set8"}
        tzm{"set6"}
        ug{"set8"}
        uk{"set30"}
        ur{"set3"}
        uz{"set8"}
        ve{"set8"}
        vi{"set0"}
        vo{"set8"}
        vun{"set8"}
        wa{"set5"}
        wae{"set8"}
        wo{"set0"}
        xh{"set8"}
        xog{"set8"}
        yi{"set3"}
        yo{"set0"}
        zh{"set0"}
        zu{"set1"}
    }
    locales_ordinals{
        af{"set36"}
        am{"set36"}
        ar{"set36"}
        as{"set52"}
        az{"set50"}
        bg{"set36"}
        bn{"set52"}
        bs{"set36"}
        ca{"set48"}
        ce{"set36"}
        cs{"set36"}
        cy{"set53"}
        da{"set36"}
        de{"set36"}
        dsb{"set36"}
        el{"set36"}
        en{"set46"}
        es{"set36"}
        et{"set36"}
        eu{"set36"}
        fa{"set36"}
        fi{"set36"}
        fil{"set38"}
        fr{"set38"}
        fy{"set36"}
        gl{"set36"}
        gu{"set51"}
        he{"set36"}
        hi{"set51"}
        hr{"set36"}
        hsb{"set36"}
        hu{"set39"}
        hy{"set38"}
        id{"set36"}
        in{"set36"}
        is{"set36"}
        it{"set43"}
        iw{"set36"}
        ja{"set36"}
        ka{"set44"}
        kk{"set42"}
        km{"set36"}
        kn{"set36"}
        ko{"set36"}
        ky{"set36"}
        lo{"set38"}
        lt{"set36"}
        lv{"set36"}
        mk{"set49"}
        ml{"set36"}
        mn{"set36"}
        mo{"set38"}
        mr{"set47"}
        ms{"set38"}
        my{"set36"}
        nb{"set36"}
        ne{"set40"}
        nl{"set36"}
        pa{"set36"}
        pl{"set36"}
        prg{"set36"}
        pt{"set36"}
        ro{"set38"}
        root{"set36"}
        ru{"set36"}
        sh{"set36"}
        si{"set36"}
        sk{"set36"}
        sl{"set36"}
        sq{"set45"}
        sr{"set36"}
        sv{"set37"}
        sw{"set36"}
        ta{"set36"}
        te{"set36"}
        th{"set36"}
        tl{"set38"}
        tr{"set36"}
        uk{"set41"}
        ur{"set36"}
        uz{"set36"}
        vi{"set38"}
        zh{"set36"}
        zu{"set36"}
    }
    rules{
        set0{
            other{
                " @integer 0~15, 100, 1000, 10000, 100000, 1000000, … @decimal 0.0~1."
                "5, 10.0, 100.0, 1000.0, 10000.0, 100000.0, 1000000.0, …"
            }
        }
        set1{
            one{"i = 0 or n = 1 @integer 0, 1 @decimal 0.0~1.0, 0.00~0.04"}
            other{
                " @integer 2~17, 100, 1000, 10000, 100000, 1000000, … @decimal 1.1~2."
                "6, 10.0, 100.0, 1000.0, 10000.0, 100000.0, 1000000.0, …"
            }
        }
        set10{
            one{"n = 1 or t != 0 and i = 0,1 @integer 1 @decimal 0.1~1.6"}
            other{
                " @integer 0, 2~16, 100, 1000, 10000, 100000, 1000000, … @decimal 0.0"
                ", 2.0~3.4, 10.0, 100.0, 1000.0, 10000.0, 100000.0, 1000000.0, …"
            }
        }
        set11{
            one{
                "t = 0 and i % 10 = 1 and i % 100 != 11 or t != 0 @integer 1, 21, 31,"
                " 41, 51, 61, 71, 81, 101, 1001, … @decimal 0.1~1.6, 10.1, 100.1, 100"
                "0.1, …"
            }
            other{
                " @integer 0, 2~16, 100, 1000, 10000, 100000, 1000000, … @decimal 0.0"
                ", 2.0, 3.0, 4.0, 5.0, 6.0, 7.0, 8.0, 10.0, 100.0, 1000.0, 10000.0, 1"
                "00000.0, 1000000.0, …"
            }
        }
        set12{
            one{
                "v = 0 and i % 10 = 1 or f % 10 = 1 @integer 1, 11, 21, 31, 41, 51, 6"
                "1, 71, 101, 1001, … @decimal 0.1, 1.1, 2.1, 3.1, 4.1, 5.1, 6.1, 7.1,"
                " 10.1, 100.1, 1000.1, …"
            }
            other{
                " @integer 0, 2~10, 12~17, 100, 1000, 10000, 100000, 1000000, … @deci"
                "mal 0.0, 0.2~1.0, 1.2~1.7, 10.0, 100.0, 1000.0, 10000.0, 100000.0, 1"
                "000000.0, …"
            }
        }
        set13{
            one{
                "v = 0 and i = 1,2,3 or v = 0 and i % 10 != 4,6,9 or v != 0 and f % 1"
                "0 != 4,6,9 @integer 0~3, 5, 7, 8, 10~13, 15, 17, 18, 20, 21, 100, 10"
                "00, 10000, 100000, 1000000, … @decimal 0.0~0.3, 0.5, 0.7, 0.8, 1.0~1"
                ".3, 1.5, 1.7, 1.8, 2.0, 2.1, 10.0, 100.0, 1000.0, 10000.0, 100000.0,"
                " 1000000.0, …"
            }
            other{
                " @integer 4, 6, 9, 14, 16, 19, 24, 26, 104, 1004, … @decimal 0.4, 0."
                "6, 0.9, 1.4, 1.6, 1.9, 2.4, 2.6, 10.4, 100.4, 1000.4, …"
            }
        }
        set14{
            one{
                "n % 10 = 1 and n % 100 != 11 or v = 2 and f % 10 = 1 and f % 100 != "
                "11 or v != 2 and f % 10 = 1 @integer 1, 21, 31, 41, 51, 61, 71, 81, "
                "101, 1001, … @decimal 0.1, 1.0, 1.1, 2.1, 3.1, 4.1, 5.1, 6.1, 7.1, 1"
                "0.1, 100.1, 1000.1, …"
            }
            other{
                " @integer 2~9, 22~29, 102, 1002, … @decimal 0.2~0.9, 1.2~1.9, 10.2, "
                "100.2, 1000.2, …"
            }
            zero{
                "n % 10 = 0 or n % 100 = 11..19 or v = 2 and f % 100 = 11..19 @intege"
                "r 0, 10~20, 30, 40, 50, 60, 100, 1000, 10000, 100000, 1000000, … @de"
                "cimal 0.0, 10.0, 11.0, 12.0, 13.0, 14.0, 15.0, 16.0, 100.0, 1000.0, "
                "10000.0, 100000.0, 1000000.0, …"
            }
        }
        set15{
            one{"i = 0,1 and n != 0 @integer 1 @decimal 0.1~1.6"}
            other{
                " @integer 2~17, 100, 1000, 10000, 100000, 1000000, … @decimal 2.0~3."
                "5, 10.0, 100.0, 1000.0, 10000.0, 100000.0, 1000000.0, …"
            }
            zero{"n = 0 @integer 0 @decimal 0.0, 0.00, 0.000, 0.0000"}
        }
        set16{
            one{"n = 1 @integer 1 @decimal 1.0, 1.00, 1.000, 1.0000"}
            other{
                " @integer 2~17, 100, 1000, 10000, 100000, 1000000, … @decimal 0.1~0."
                "9, 1.1~1.7, 10.0, 100.0, 1000.0, 10000.0, 100000.0, 1000000.0, …"
            }
            zero{"n = 0 @integer 0 @decimal 0.0, 0.00, 0.000, 0.0000"}
        }
        set17{
            one{"n = 1 @integer 1 @decimal 1.0, 1.00, 1.000, 1.0000"}
            other{
                " @integer 0, 3~17, 100, 1000, 10000, 100000, 1000000, … @decimal 0.0"
                "~0.9, 1.1~1.6, 10.0, 100.0, 1000.0, 10000.0, 100000.0, 1000000.0, …"
            }
            two{"n = 2 @integer 2 @decimal 2.0, 2.00, 2.000, 2.0000"}
        }
        set18{
            few{
                "n = 2..10 @integer 2~10 @decimal 2.0, 3.0, 4.0, 5.0, 6.0, 7.0, 8.0, "
                "9.0, 10.0, 2.00, 3.00, 4.00, 5.00, 6.00, 7.00, 8.00"
            }
            one{"i = 0 or n = 1 @integer 0, 1 @decimal 0.0~1.0, 0.00~0.04"}
            other{
                " @integer 11~26, 100, 1000, 10000, 100000, 1000000, … @decimal 1.1~1"
                ".9, 2.1~2.7, 10.1, 100.0, 1000.0, 10000.0, 100000.0, 1000000.0, …"
            }
        }
        set19{
            few{
                "v != 0 or n = 0 or n != 1 and n % 100 = 1..19 @integer 0, 2~16, 101,"
                " 1001, … @decimal 0.0~1.5, 10.0, 100.0, 1000.0, 10000.0, 100000.0, 1"
                "000000.0, …"
            }
            one{"i = 1 and v = 0 @integer 1"}
            other{" @integer 20~35, 100, 1000, 10000, 100000, 1000000, …"}
        }
        set2{
            one{"i = 0,1 @integer 0, 1 @decimal 0.0~1.5"}
            other{
                " @integer 2~17, 100, 1000, 10000, 100000, 1000000, … @decimal 2.0~3."
                "5, 10.0, 100.0, 1000.0, 10000.0, 100000.0, 1000000.0, …"
            }
        }
        set20{
            few{
                "v = 0 and i % 10 = 2..4 and i % 100 != 12..14 or f % 10 = 2..4 and f"
                " % 100 != 12..14 @integer 2~4, 22~24, 32~34, 42~44, 52~54, 62, 102, "
                "1002, … @decimal 0.2~0.4, 1.2~1.4, 2.2~2.4, 3.2~3.4, 4.2~4.4, 5.2, 1"
                "0.2, 100.2, 1000.2, …"
            }
            one{
                "v = 0 and i % 10 = 1 and i % 100 != 11 or f % 10 = 1 and f % 100 != "
                "11 @integer 1, 21, 31, 41, 51, 61, 71, 81, 101, 1001, … @decimal 0.1"
                ", 1.1, 2.1, 3.1, 4.1, 5.1, 6.1, 7.1, 10.1, 100.1, 1000.1, …"
            }
            other{
                " @integer 0, 5~19, 100, 1000, 10000, 100000, 1000000, … @decimal 0.0"
                ", 0.5~1.0, 1.5~2.0, 2.5~2.7, 10.0, 100.0, 1000.0, 10000.0, 100000.0,"
                " 1000000.0, …"
            }
        }
        set21{
            few{
                "n = 3..10,13..19 @integer 3~10, 13~19 @decimal 3.0, 4.0, 5.0, 6.0, 7"
                ".0, 8.0, 9.0, 10.0, 13.0, 14.0, 15.0, 16.0, 17.0, 18.0, 19.0, 3.00"
            }
            one{
                "n = 1,11 @integer 1, 11 @decimal 1.0, 11.0, 1.00, 11.00, 1.000, 11.0"
                "00, 1.0000"
            }
            other{
                " @integer 0, 20~34, 100, 1000, 10000, 100000, 1000000, … @decimal 0."
                "0~0.9, 1.1~1.6, 10.1, 100.0, 1000.0, 10000.0, 100000.0, 1000000.0, …"
            }
            two{
                "n = 2,12 @integer 2, 12 @decimal 2.0, 12.0, 2.00, 12.00, 2.000, 12.0"
                "00, 2.0000"
            }
        }
        set22{
            few{
                "v = 0 and i % 100 = 3..4 or v != 0 @integer 3, 4, 103, 104, 203, 204"
                ", 303, 304, 403, 404, 503, 504, 603, 604, 703, 704, 1003, … @decimal"
                " 0.0~1.5, 10.0, 100.0, 1000.0, 10000.0, 100000.0, 1000000.0, …"
            }
            one{
                "v = 0 and i % 100 = 1 @integer 1, 101, 201, 301, 401, 501, 601, 701,"
                " 1001, …"
            }
            other{" @integer 0, 5~19, 100, 1000, 10000, 100000, 1000000, …"}
            two{
                "v = 0 and i % 100 = 2 @integer 2, 102, 202, 302, 402, 502, 602, 702,"
                " 1002, …"
            }
        }
        set23{
            few{
                "v = 0 and i % 100 = 3..4 or f % 100 = 3..4 @integer 3, 4, 103, 104, "
                "203, 204, 303, 304, 403, 404, 503, 504, 603, 604, 703, 704, 1003, … "
                "@decimal 0.3, 0.4, 1.3, 1.4, 2.3, 2.4, 3.3, 3.4, 4.3, 4.4, 5.3, 5.4,"
                " 6.3, 6.4, 7.3, 7.4, 10.3, 100.3, 1000.3, …"
            }
            one{
                "v = 0 and i % 100 = 1 or f % 100 = 1 @integer 1, 101, 201, 301, 401,"
                " 501, 601, 701, 1001, … @decimal 0.1, 1.1, 2.1, 3.1, 4.1, 5.1, 6.1, "
                "7.1, 10.1, 100.1, 1000.1, …"
            }
            other{
                " @integer 0, 5~19, 100, 1000, 10000, 100000, 1000000, … @decimal 0.0"
                ", 0.5~1.0, 1.5~2.0, 2.5~2.7, 10.0, 100.0, 1000.0, 10000.0, 100000.0,"
                " 1000000.0, …"
            }
            two{
                "v = 0 and i % 100 = 2 or f % 100 = 2 @integer 2, 102, 202, 302, 402,"
                " 502, 602, 702, 1002, … @decimal 0.2, 1.2, 2.2, 3.2, 4.2, 5.2, 6.2, "
                "7.2, 10.2, 100.2, 1000.2, …"
            }
        }
        set24{
            many{
                "v = 0 and n != 0..10 and n % 10 = 0 @integer 20, 30, 40, 50, 60, 70,"
                " 80, 90, 100, 1000, 10000, 100000, 1000000, …"
            }
            one{"i = 1 and v = 0 @integer 1"}
            other{
                " @integer 0, 3~17, 101, 1001, … @decimal 0.0~1.5, 10.0, 100.0, 1000."
                "0, 10000.0, 100000.0, 1000000.0, …"
            }
            two{"i = 2 and v = 0 @integer 2"}
        }
        set25{
            few{"i = 2..4 and v = 0 @integer 2~4"}
            many{
                "v != 0   @decimal 0.0~1.5, 10.0, 100.0, 1000.0, 10000.0, 100000.0, 1"
                "000000.0, …"
            }
            one{"i = 1 and v = 0 @integer 1"}
            other{" @integer 0, 5~19, 100, 1000, 10000, 100000, 1000000, …"}
        }
        set26{
            few{
                "v = 0 and i % 10 = 2..4 and i % 100 != 12..14 @integer 2~4, 22~24, 3"
                "2~34, 42~44, 52~54, 62, 102, 1002, …"
            }
            many{
                "v = 0 and i != 1 and i % 10 = 0..1 or v = 0 and i % 10 = 5..9 or v ="
                " 0 and i % 100 = 12..14 @integer 0, 5~19, 100, 1000, 10000, 100000, "
                "1000000, …"
            }
            one{"i = 1 and v = 0 @integer 1"}
            other{
                "   @decimal 0.0~1.5, 10.0, 100.0, 1000.0, 10000.0, 100000.0, 1000000"
                ".0, …"
            }
        }
        set27{
            few{
                "n % 10 = 2..4 and n % 100 != 12..14 @integer 2~4, 22~24, 32~34, 42~4"
                "4, 52~54, 62, 102, 1002, … @decimal 2.0, 3.0, 4.0, 22.0, 23.0, 24.0,"
                " 32.0, 33.0, 102.0, 1002.0, …"
            }
            many{
                "n % 10 = 0 or n % 10 = 5..9 or n % 100 = 11..14 @integer 0, 5~19, 10"
                "0, 1000, 10000, 100000, 1000000, … @decimal 0.0, 5.0, 6.0, 7.0, 8.0,"
                " 9.0, 10.0, 11.0, 100.0, 1000.0, 10000.0, 100000.0, 1000000.0, …"
            }
            one{
                "n % 10 = 1 and n % 100 != 11 @integer 1, 21, 31, 41, 51, 61, 71, 81,"
                " 101, 1001, … @decimal 1.0, 21.0, 31.0, 41.0, 51.0, 61.0, 71.0, 81.0"
                ", 101.0, 1001.0, …"
            }
            other{"   @decimal 0.1~0.9, 1.1~1.7, 10.1, 100.1, 1000.1, …"}
        }
        set28{
            few{
                "n % 10 = 2..9 and n % 100 != 11..19 @integer 2~9, 22~29, 102, 1002, "
                "… @decimal 2.0, 3.0, 4.0, 5.0, 6.0, 7.0, 8.0, 9.0, 22.0, 102.0, 1002"
                ".0, …"
            }
            many{"f != 0   @decimal 0.1~0.9, 1.1~1.7, 10.1, 100.1, 1000.1, …"}
            one{
                "n % 10 = 1 and n % 100 != 11..19 @integer 1, 21, 31, 41, 51, 61, 71,"
                " 81, 101, 1001, … @decimal 1.0, 21.0, 31.0, 41.0, 51.0, 61.0, 71.0, "
                "81.0, 101.0, 1001.0, …"
            }
            other{
                " @integer 0, 10~20, 30, 40, 50, 60, 100, 1000, 10000, 100000, 100000"
                "0, … @decimal 0.0, 10.0, 11.0, 12.0, 13.0, 14.0, 15.0, 16.0, 100.0, "
                "1000.0, 10000.0, 100000.0, 1000000.0, …"
            }
        }
        set29{
            few{
                "n = 0 or n % 100 = 2..10 @integer 0, 2~10, 102~107, 1002, … @decimal"
                " 0.0, 2.0, 3.0, 4.0, 5.0, 6.0, 7.0, 8.0, 10.0, 102.0, 1002.0, …"
            }
            many{
                "n % 100 = 11..19 @integer 11~19, 111~117, 1011, … @decimal 11.0, 12."
                "0, 13.0, 14.0, 15.0, 16.0, 17.0, 18.0, 111.0, 1011.0, …"
            }
            one{"n = 1 @integer 1 @decimal 1.0, 1.00, 1.000, 1.0000"}
            other{
                " @integer 20~35, 100, 1000, 10000, 100000, 1000000, … @decimal 0.1~0"
                ".9, 1.1~1.7, 10.1, 100.0, 1000.0, 10000.0, 100000.0, 1000000.0, …"
            }
        }
        set3{
            one{"i = 1 and v = 0 @integer 1"}
            other{
                " @integer 0, 2~16, 100, 1000, 10000, 100000, 1000000, … @decimal 0.0"
                "~1.5, 10.0, 100.0, 1000.0, 10000.0, 100000.0, 1000000.0, …"
            }
        }
        set30{
            few{
                "v = 0 and i % 10 = 2..4 and i % 100 != 12..14 @integer 2~4, 22~24, 3"
                "2~34, 42~44, 52~54, 62, 102, 1002, …"
            }
            many{
                "v = 0 and i % 10 = 0 or v = 0 and i % 10 = 5..9 or v = 0 and i % 100"
                " = 11..14 @integer 0, 5~19, 100, 1000, 10000, 100000, 1000000, …"
            }
            one{
                "v = 0 and i % 10 = 1 and i % 100 != 11 @integer 1, 21, 31, 41, 51, 6"
                "1, 71, 81, 101, 1001, …"
            }
            other{
                "   @decimal 0.0~1.5, 10.0, 100.0, 1000.0, 10000.0, 100000.0, 1000000"
                ".0, …"
            }
        }
        set31{
            few{
                "n % 10 = 3..4,9 and n % 100 != 10..19,70..79,90..99 @integer 3, 4, 9"
                ", 23, 24, 29, 33, 34, 39, 43, 44, 49, 103, 1003, … @decimal 3.0, 4.0"
                ", 9.0, 23.0, 24.0, 29.0, 33.0, 34.0, 103.0, 1003.0, …"
            }
            many{
                "n != 0 and n % 1000000 = 0 @integer 1000000, … @decimal 1000000.0, 1"
                "000000.00, 1000000.000, …"
            }
            one{
                "n % 10 = 1 and n % 100 != 11,71,91 @integer 1, 21, 31, 41, 51, 61, 8"
                "1, 101, 1001, … @decimal 1.0, 21.0, 31.0, 41.0, 51.0, 61.0, 81.0, 10"
                "1.0, 1001.0, …"
            }
            other{
                " @integer 0, 5~8, 10~20, 100, 1000, 10000, 100000, … @decimal 0.0~0."
                "9, 1.1~1.6, 10.0, 100.0, 1000.0, 10000.0, 100000.0, …"
            }
            two{
                "n % 10 = 2 and n % 100 != 12,72,92 @integer 2, 22, 32, 42, 52, 62, 8"
                "2, 102, 1002, … @decimal 2.0, 22.0, 32.0, 42.0, 52.0, 62.0, 82.0, 10"
                "2.0, 1002.0, …"
            }
        }
        set32{
            few{
                "n = 3..6 @integer 3~6 @decimal 3.0, 4.0, 5.0, 6.0, 3.00, 4.00, 5.00,"
                " 6.00, 3.000, 4.000, 5.000, 6.000, 3.0000, 4.0000, 5.0000, 6.0000"
            }
            many{
                "n = 7..10 @integer 7~10 @decimal 7.0, 8.0, 9.0, 10.0, 7.00, 8.00, 9."
                "00, 10.00, 7.000, 8.000, 9.000, 10.000, 7.0000, 8.0000, 9.0000, 10.0"
                "000"
            }
            one{"n = 1 @integer 1 @decimal 1.0, 1.00, 1.000, 1.0000"}
            other{
                " @integer 0, 11~25, 100, 1000, 10000, 100000, 1000000, … @decimal 0."
                "0~0.9, 1.1~1.6, 10.1, 100.0, 1000.0, 10000.0, 100000.0, 1000000.0, …"
            }
            two{"n = 2 @integer 2 @decimal 2.0, 2.00, 2.000, 2.0000"}
        }
        set33{
            few{
                "v = 0 and i % 100 = 0,20,40,60,80 @integer 0, 20, 40, 60, 80, 100, 1"
                "20, 140, 1000, 10000, 100000, 1000000, …"
            }
            many{
                "v != 0   @decimal 0.0~1.5, 10.0, 100.0, 1000.0, 10000.0, 100000.0, 1"
                "000000.0, …"
            }
            one{
                "v = 0 and i % 10 = 1 @integer 1, 11, 21, 31, 41, 51, 61, 71, 101, 10"
                "01, …"
            }
            other{" @integer 3~10, 13~19, 23, 103, 1003, …"}
            two{
                "v = 0 and i % 10 = 2 @integer 2, 12, 22, 32, 42, 52, 62, 72, 102, 10"
                "02, …"
            }
        }
        set34{
            few{
                "n % 100 = 3..10 @integer 3~10, 103~110, 1003, … @decimal 3.0, 4.0, 5"
                ".0, 6.0, 7.0, 8.0, 9.0, 10.0, 103.0, 1003.0, …"
            }
            many{
                "n % 100 = 11..99 @integer 11~26, 111, 1011, … @decimal 11.0, 12.0, 1"
                "3.0, 14.0, 15.0, 16.0, 17.0, 18.0, 111.0, 1011.0, …"
            }
            one{"n = 1 @integer 1 @decimal 1.0, 1.00, 1.000, 1.0000"}
            other{
                " @integer 100~102, 200~202, 300~302, 400~402, 500~502, 600, 1000, 10"
                "000, 100000, 1000000, … @decimal 0.1~0.9, 1.1~1.7, 10.1, 100.0, 1000"
                ".0, 10000.0, 100000.0, 1000000.0, …"
            }
            two{"n = 2 @integer 2 @decimal 2.0, 2.00, 2.000, 2.0000"}
            zero{"n = 0 @integer 0 @decimal 0.0, 0.00, 0.000, 0.0000"}
        }
        set35{
            few{"n = 3 @integer 3 @decimal 3.0, 3.00, 3.000, 3.0000"}
            many{"n = 6 @integer 6 @decimal 6.0, 6.00, 6.000, 6.0000"}
            one{"n = 1 @integer 1 @decimal 1.0, 1.00, 1.000, 1.0000"}
            other{
                " @integer 4, 5, 7~20, 100, 1000, 10000, 100000, 1000000, … @decimal "
                "0.1~0.9, 1.1~1.7, 10.0, 100.0, 1000.0, 10000.0, 100000.0, 1000000.0,"
                " …"
            }
            two{"n = 2 @integer 2 @decimal 2.0, 2.00, 2.000, 2.0000"}
            zero{"n = 0 @integer 0 @decimal 0.0, 0.00, 0.000, 0.0000"}
        }
        set36{
            other{" @integer 0~15, 100, 1000, 10000, 100000, 1000000, …"}
        }
        set37{
            one{
                "n % 10 = 1,2 and n % 100 != 11,12 @integer 1, 2, 21, 22, 31, 32, 41,"
                " 42, 51, 52, 61, 62, 71, 72, 81, 82, 101, 1001, …"
            }
            other{" @integer 0, 3~17, 100, 1000, 10000, 100000, 1000000, …"}
        }
        set38{
            one{"n = 1 @integer 1"}
            other{" @integer 0, 2~16, 100, 1000, 10000, 100000, 1000000, …"}
        }
        set39{
            one{"n = 1,5 @integer 1, 5"}
            other{" @integer 0, 2~4, 6~17, 100, 1000, 10000, 100000, 1000000, …"}
        }
        set4{
            one{
                "n = 0,1 or i = 0 and f = 1 @integer 0, 1 @decimal 0.0, 0.1, 1.0, 0.0"
                "0, 0.01, 1.00, 0.000, 0.001, 1.000, 0.0000, 0.0001, 1.0000"
            }
            other{
                " @integer 2~17, 100, 1000, 10000, 100000, 1000000, … @decimal 0.2~0."
                "9, 1.1~1.8, 10.0, 100.0, 1000.0, 10000.0, 100000.0, 1000000.0, …"
            }
        }
        set40{
            one{"n = 1..4 @integer 1~4"}
            other{" @integer 0, 5~19, 100, 1000, 10000, 100000, 1000000, …"}
        }
        set41{
            few{
                "n % 10 = 3 and n % 100 != 13 @integer 3, 23, 33, 43, 53, 63, 73, 83,"
                " 103, 1003, …"
            }
            other{" @integer 0~2, 4~16, 100, 1000, 10000, 100000, 1000000, …"}
        }
        set42{
            many{
                "n % 10 = 6 or n % 10 = 9 or n % 10 = 0 and n != 0 @integer 6, 9, 10,"
                " 16, 19, 20, 26, 29, 30, 36, 39, 40, 100, 1000, 10000, 100000, 10000"
                "00, …"
            }
            other{" @integer 0~5, 7, 8, 11~15, 17, 18, 21, 101, 1001, …"}
        }
        set43{
            many{"n = 11,8,80,800 @integer 8, 11, 80, 800"}
            other{" @integer 0~7, 9, 10, 12~17, 100, 1000, 10000, 100000, 1000000, …"}
        }
        set44{
            many{"i = 0 or i % 100 = 2..20,40,60,80 @integer 0, 2~16, 102, 1002, …"}
            one{"i = 1 @integer 1"}
            other{" @integer 21~36, 100, 1000, 10000, 100000, 1000000, …"}
        }
        set45{
            many{
                "n % 10 = 4 and n % 100 != 14 @integer 4, 24, 34, 44, 54, 64, 74, 84,"
                " 104, 1004, …"
            }
            one{"n = 1 @integer 1"}
            other{" @integer 0, 2, 3, 5~17, 100, 1000, 10000, 100000, 1000000, …"}
        }
        set46{
            few{
                "n % 10 = 3 and n % 100 != 13 @integer 3, 23, 33, 43, 53, 63, 73, 83,"
                " 103, 1003, …"
            }
            one{
                "n % 10 = 1 and n % 100 != 11 @integer 1, 21, 31, 41, 51, 61, 71, 81,"
                " 101, 1001, …"
            }
            other{" @integer 0, 4~18, 100, 1000, 10000, 100000, 1000000, …"}
            two{
                "n % 10 = 2 and n % 100 != 12 @integer 2, 22, 32, 42, 52, 62, 72, 82,"
                " 102, 1002, …"
            }
        }
        set47{
            few{"n = 4 @integer 4"}
            one{"n = 1 @integer 1"}
            other{" @integer 0, 5~19, 100, 1000, 10000, 100000, 1000000, …"}
            two{"n = 2,3 @integer 2, 3"}
        }
        set48{
            few{"n = 4 @integer 4"}
            one{"n = 1,3 @integer 1, 3"}
            other{" @integer 0, 5~19, 100, 1000, 10000, 100000, 1000000, …"}
            two{"n = 2 @integer 2"}
        }
        set49{
            many{
                "i % 10 = 7,8 and i % 100 != 17,18 @integer 7, 8, 27, 28, 37, 38, 47,"
                " 48, 57, 58, 67, 68, 77, 78, 87, 88, 107, 1007, …"
            }
            one{
                "i % 10 = 1 and i % 100 != 11 @integer 1, 21, 31, 41, 51, 61, 71, 81,"
                " 101, 1001, …"
            }
            other{" @integer 0, 3~6, 9~19, 100, 1000, 10000, 100000, 1000000, …"}
            two{
                "i % 10 = 2 and i % 100 != 12 @integer 2, 22, 32, 42, 52, 62, 72, 82,"
                " 102, 1002, …"
            }
        }
        set5{
            one{
                "n = 0..1 @integer 0, 1 @decimal 0.0, 1.0, 0.00, 1.00, 0.000, 1.000, "
                "0.0000, 1.0000"
            }
            other{
                " @integer 2~17, 100, 1000, 10000, 100000, 1000000, … @decimal 0.1~0."
                "9, 1.1~1.7, 10.0, 100.0, 1000.0, 10000.0, 100000.0, 1000000.0, …"
            }
        }
        set50{
            few{
                "i % 10 = 3,4 or i % 1000 = 100,200,300,400,500,600,700,800,900 @inte"
                "ger 3, 4, 13, 14, 23, 24, 33, 34, 43, 44, 53, 54, 63, 64, 73, 74, 10"
                "0, 1003, …"
            }
            many{
                "i = 0 or i % 10 = 6 or i % 100 = 40,60,90 @integer 0, 6, 16, 26, 36,"
                " 40, 46, 56, 106, 1006, …"
            }
            one{
                "i % 10 = 1,2,5,7,8 or i % 100 = 20,50,70,80 @integer 1, 2, 5, 7, 8, "
                "11, 12, 15, 17, 18, 20~22, 25, 101, 1001, …"
            }
            other{
                " @integer 9, 10, 19, 29, 30, 39, 49, 59, 69, 79, 109, 1000, 10000, 1"
                "00000, 1000000, …"
            }
        }
        set51{
            few{"n = 4 @integer 4"}
            many{"n = 6 @integer 6"}
            one{"n = 1 @integer 1"}
            other{" @integer 0, 5, 7~20, 100, 1000, 10000, 100000, 1000000, …"}
            two{"n = 2,3 @integer 2, 3"}
        }
        set52{
            few{"n = 4 @integer 4"}
            many{"n = 6 @integer 6"}
            one{"n = 1,5,7,8,9,10 @integer 1, 5, 7~10"}
            other{" @integer 0, 11~25, 100, 1000, 10000, 100000, 1000000, …"}
            two{"n = 2,3 @integer 2, 3"}
        }
        set53{
            few{"n = 3,4 @integer 3, 4"}
            many{"n = 5,6 @integer 5, 6"}
            one{"n = 1 @integer 1"}
            other{" @integer 10~25, 100, 1000, 10000, 100000, 1000000, …"}
            two{"n = 2 @integer 2"}
            zero{"n = 0,7,8,9 @integer 0, 7~9"}
        }
        set6{
            one{
                "n = 0..1 or n = 11..99 @integer 0, 1, 11~24 @decimal 0.0, 1.0, 11.0,"
                " 12.0, 13.0, 14.0, 15.0, 16.0, 17.0, 18.0, 19.0, 20.0, 21.0, 22.0, 2"
                "3.0, 24.0"
            }
            other{
                " @integer 2~10, 100~106, 1000, 10000, 100000, 1000000, … @decimal 0."
                "1~0.9, 1.1~1.7, 10.0, 100.0, 1000.0, 10000.0, 100000.0, 1000000.0, …"
            }
        }
        set7{
            one{
                "n = 0..2 and n != 2 @integer 0, 1 @decimal 0.0, 1.0, 0.00, 1.00, 0.0"
                "00, 1.000, 0.0000, 1.0000"
            }
            other{
                " @integer 2~17, 100, 1000, 10000, 100000, 1000000, … @decimal 0.1~0."
                "9, 1.1~1.7, 10.0, 100.0, 1000.0, 10000.0, 100000.0, 1000000.0, …"
            }
        }
        set8{
            one{"n = 1 @integer 1 @decimal 1.0, 1.00, 1.000, 1.0000"}
            other{
                " @integer 0, 2~16, 100, 1000, 10000, 100000, 1000000, … @decimal 0.0"
                "~0.9, 1.1~1.6, 10.0, 100.0, 1000.0, 10000.0, 100000.0, 1000000.0, …"
            }
        }
        set9{
            one{"n = 1 and v = 0 @integer 1"}
            other{
                " @integer 0, 2~16, 100, 1000, 10000, 100000, 1000000, … @decimal 0.0"
                "~1.5, 10.0, 100.0, 1000.0, 10000.0, 100000.0, 1000000.0, …"
            }
        }
    }
}
��������������   ﻿{
  "d MMMM yyyy 'il'",
  "yyyy' il'", 
  "LLLL' 'yyyy' il'", 
  "yyyy' il. 'q' rüb'", 
  "yyyy' il. 1 yarım il'", 
  "yyyy' il. 9 ayı'" 
}�����   ﻿{
  "d MMMM yyyy",
  "yyyy' г.'", 
  "LLLL' 'yyyy' г.'", 
  "q' квартал 'yyyy' г.'", 
  "'1 паўгоддзе 'yyyy' г.'", 
  "'9 месяцаў 'yyyy' г.'" 
}�������   ﻿{
  "dd MMMM yyyy' г.'",
  "yyyy' г.'", 
  "LLLL' 'yyyy' г.'", 
  "q'(1-во|2-ро|3-то|4-то|) тримесечие на 'yyyy' г.'", 
  "'1-во полугодие на 'yyyy' г.'", 
  "'деветмесечие на 'yyyy' г.'"
}�   ﻿{
  "EEEE, d. MMMM yyyy",
  "yyyy", 
  "LLLL' 'yyyy", 
  "q'. Quartal 'yyyy", 
  "'1. Halbjahr 'yyyy", 
  "'neun Monate des Jahres 'yyyy" 
}�������   ﻿{
  "EEEE, MMMM d, yyyy",
  "yyyy' year'", 
  "LLLL' 'yyyy", 
  "q' quarter of 'yyyy", 
  "'1-st half year of 'yyyy", 
  "'9 months of 'yyyy" 
}���   ﻿{
  "d. MMMM yyyy'.a.'",
  "yyyy'.a.'", 
  "LLLL' 'yyyy'.a.'", 
  "q'. kvartal 'yyyy'.a.'", 
  "'1. poolaasta 'yyyy'.a.'", 
  "'9 kuud 'yyyy'.a.'" 
}��������������   ﻿{
  "d. MMMM yyyy",
  "yyyy' v.'", 
  "LLLL' 'yyyy' v.'", 
  "q' vuosineljännes 'yyyy' v.'", 
  "'1 vuosipuolisko 'yyyy' v.'", 
  "'9 kuukautta 'yyyy' v.'" 
}����   ﻿{
  "dd'/'MM'/'yyyy",
  "yyyy", 
  "LLLL' 'yyyy", 
  "q'er trimestre 'yyyy", 
  "'1er semestre 'yyyy", 
  "'9 mois 'yyyy"
}��������   ﻿{
  "yyyy.MMMM.d",
  "yyyy", 
  "LLLL' 'yyyy", 
  "yyyy'.'q'. negyedév'", 
  "yyyy'.I. félév", 
  "yyyy'.IX. hónap'" 
}��������   ﻿{
  "yyyy 'წლის' dd MM',' EEEE",
  "yyyy' წ.'", 
  "LLLL' 'yyyy' წ.'", 
  "q' კვარტალი 'yyyy' წ.'", 
  "'1 ნახევარწელი 'yyyy' წ.'", 
  "'9 თვე 'yyyy' წ.'" 
}��������   ﻿{
  "d MMMM yyyy 'ж.'",
  "yyyy' ж.'", 
  "LLLL' 'yyyy' ж.'", 
  "q' тоқсан 'yyyy' ж.'", 
  "'1 жарты жыл 'yyyy' ж.'", 
  "'9 ай 'yyyy' ж.'" 
}��������������   ﻿{
  "yyyy 'm.' MMMM d 'd.'",
  "yyyy' m.'", 
  "LLLL' 'yyyy' m.'", 
  "yyyy' m. 'q'-as ketvirtis'", 
  "yyyy' m. 1-as pusmetis'", 
  "yyyy' m. 9 mėnesius'" 
}����   ﻿{
  "EEEE, yyyy'. gada 'd. MMMM",
  "yyyy'.g.'", 
  "yyyy'.g. 'LLLL", 
  "yyyy'.g. 'q'.ceturksnis'", 
  "yyyy'.g. 1.pusgads'", 
  "yyyy'.g. 9 mēneši'" 
}��������   ﻿{
  "d MMMM yyyy 'r.'",
  "yyyy' r.'", 
  "LLLL' 'yyyy' r.'", 
  "q' kwartał 'yyyy' r.'", 
  "'1 półrocze 'yyyy' r.'", 
  "'9 miesięcy 'yyyy' r.'" 
}����������   ﻿{
  "d MMMM yyyy",
  "'anul 'yyyy", 
  "LLLL' 'yyyy", 
  "q' trimestru 'yyyy", 
  "'I semestru 'yyyy", 
  "'9 luni 'yyyy" 
}�������   ﻿{
  "d MMMM yyyy 'г.'",
  "yyyy' г.'", 
  "LLLL' 'yyyy' г.'", 
  "q' квартал 'yyyy' г.'", 
  "'1 полугодие 'yyyy' г.'", 
  "'9 месяцев 'yyyy' г.'" 
}�   ﻿{
  "d MMMM yyyy 'ý.'",
  "yyyy' ý.'", 
  "LLLL' 'yyyy' ý.'", 
  "yyyy'-nji ýylyn 'q'-nji çaryýek'", 
  "yyyy'-nji ýylyn 1-nji ýarym ýylygy'", 
  "yyyy'-nji ýylyn 9 aýy'" 
}�����������   ﻿{
  "d LLLL yyyy 'yılı'",
  "yyyy 'yılı'", 
  "LLLL yyyy 'yılı'", 
  "yyyy 'yılı' q'.çeyrek' ", 
  "yyyy 'yılı 1.altı aylık'", 
  "yyyy 'yılı 9 aylık'" 
}���������   ﻿{
  "d MMMM yyyy' р.'",
  "yyyy' р.'", 
  "LLLL' 'yyyy' р.'", 
  "q' квартал 'yyyy' р.'", 
  "'1 півріччя 'yyyy' р.'", 
  "'9 місяців 'yyyy' р.'" 
}���   ﻿{
  "d LLLL 'năm' yyyy",
  "'năm' yyyy", 
  "LLLL yyyy", 
  "'Quý' q 'năm' yyyy", 
  "'Nửa đầu năm' yyyy", 
  "'9 tháng năm' yyyy" 
}��  ﻿{
  { "şi" },
  { "zero" }, 
  { "unu", "una", "un", "o" }, 
  { "doi", "două" },
  { "trei", "patru", "cinci", "şase", "şapte", "opt", "nouă" },
  { "unsprezece", 
    {"doisprezece", "douăsprezece" },
     "treisprezece", "paisprezece", "cincisprezece", "şaisprezece", "şaptesprezece", "optsprezece", "nouăsprezece" },
  { "zece", "douăzeci", "treizeci", "patruzeci", "cincizeci", "şaizeci", "şaptezeci", "optzeci", "nouăzeci" },
  { "sută", "două sute", "trei sute", "patru sute", "cinci sute", "şase sute", "şapte sute", "opt sute", "nouă sute" },
  { "mie", "mii", 1 },
  { "milion", "milioane", 0 },
  { "miliard", "miliarde", 0 },
  { "trilion", "trilioane", 0 },
  { "quadrilion", "quadrilioane", 0 }
}�9  ﻿{
  { "ноль" }, 
  { "один", "одна", "одно" }, 
  { "два", "две" },
  { "три", "четыре", "пять", "шесть", "семь", "восемь", "девять" },
  { "одиннадцать", "двенадцать", "тринадцать", "четырнадцать", "пятнадцать", "шестнадцать", "семнадцать", "восемнадцать", "девятнадцать" },
  { "десять", "двадцать", "тридцать", "сорок", "пятьдесят", "шестьдесят", "семьдесят", "восемьдесят", "девяносто" },
  { "сто", "двести", "триста", "четыреста", "пятьсот", "шестьсот", "семьсот", "восемьсот", "девятьсот" },
  { "тысяча", "тысячи", "тысяч", 1 },
  { "миллион", "миллиона", "миллионов", 0 },
  { "миллиард", "миллиарда", "миллиардов", 0 },
  { "триллион", "триллиона", "триллионов", 0 }
}���  ﻿{
  { "nol", "bir", "iki", "üç", "dört", "bäş", "alty", "ýedi", "sekiz", "dokuz" },
  { "on", "ýigrimi", "otuz", "kyrk", "elli", "altmyş", "ýetmiş", "segsen", "togsan" },
  { "ýuz" },
  { "mün" },
  { "million" },
  { "milliard" },
  { "trillion" }
}�����������  ﻿{
  { "sıfır" , "bir", "İki", "üç", "dört", "beş", "altı", "yedi", "sekiz", "dokuz" },
  { "on", "yirmi", "otuz", "kırk", "elli", "altmış", "yetmiş", "seksen", "doksan" },
  { "yüz", "bin", "milyon", "milyar", "trilyon" },
  { "Solid", "Separate" },
}������������  ﻿{
   { "нуль" },
   { "один", "одна", "одно" },
   { "два", "дві" },
   { "три","чотири","п'ять","шість","сім","вісім","дев'ять" },
   { "одинадцять","дванадцять","тринадцять","чотирнадцять","п'ятнадцять","шістнадцять","сімнадцять","вісімнадцять","дев'ятнадцять" },
   { "десять", "двадцять","тридцять","сорок","п'ятдесят","шістдесят","сімдесят","вісімдесят","дев'яносто" },
   { "сто","двісті","триста","чотириста","п'ятсот","шістсот","сімсот","вісімсот","дев'ятсот" },
   { "тисяча","тисячі","тисяч", 1 },
   { "мільйон","мільйона","мільйонів", 0 },
   { "мільярд","мільярда","мільярдів", 0 },
   { "трильйон","трильйона","трильйонів", 0 }
}������   ﻿{
  { "không" , "một", "hai", "ba", "bốn", "năm", "sáu", "bảy", "tám", "chín" },
  { "mươi" , "trăm" },
  { "nghìn", "triệu", "tỷ", "nghìn tỷ" },
  { "lăm", "mốt", "linh", "mười" }
}��\L>�>i>!@�@�@�@�AB�A�?�?@x@U@�B�B�@"M ��  ��@j�6 9x8|;�;�819v< <H<�8�;�86;Z8�<�;�9�9�9:&:?:X:~9�9�:�:�:q:�:_99\;�<;H9S�b�gIir�l!�!� Y m"K"!.!� � �!�!� ,"K!�!t "S4���*�1���c@��<�4�4��D�#�*�*+@+`+x*8**�)�+�)Y*�*,�+�*�+�+�+}$8#B%&%�&$�%�%�(a%^#(}#�"�'�'B(0$�)�"j(�#�%O)�'�&_&&�%E''�&n)�(�(�"�)�&�#m''�(�%X$�$�$#7&�"&)�$���5�56<6�56�4�4��>5@5_5�5 V"��,�,-T,W-m,f.�/9-�,.).G.�, -<,�,1�0(1�-�-�/K0�1K1j0�0�.�.�-�0v-�-�0�-M/o/�.�./*/�/�/)0�/j1�#m���G�44�3�3��4�2R2q22�1�3w3�2W3�1�23/3�132d6�3�k��;c�-��T��1e���1[���*T���1W�����(Ru��=t��	9b��_DqD�D�D�DE�D�D�DE2CPCsC�A�>?�>4BqB�A�?,?F?d?|?�?B�B�CED�C�C�C'D�CD�VB�A�o�M^���	~	��<�	�	�[p����>		^	*���?�	�	Q
�	
�

4
�
�
-?^����C�B���T7�7v7�7�6�6~6�6�7 8877C8� ��h 4�T�Wm8�" 
v� 22z ��GD� D� "d[� �t�� ����� [�����G�T r59j����>�>�>R��MA1ArA��<�< >"=W2
���=�=o�i=�=�4j���=L=?qN���/�(E7E$F�E�E�EtE�EJE�EF:F�E�E�E�ERE�E]EAF F�E�E�EpE�E�EF6F�E�E�E�E�E�EeE+F
F{E�E�EiE/FFEF��A `�  `[ `�4 `�0 `U- `�/ `�! `� `a
 `� ` `/ `� `� `%' `nV `�U ` `�X `� `� `� `� `R `�1 `� `� `� `�1 `� ` `�H `� `S `uT `" `� `� `B$ `� `> `� `<  `� `jL `� `� `� `� `J `� `� `� `� `Q `M `�L `I `]3 `& `- `- `�= `? `f `/ `+ `�F `�( `� `� `� `"# `m+ `� `�> `�2 `y6 `mU `�@ `] `r$ `�E `%( `'H `�= `& `�8 `4G `�O `�P `�- `R# `rW `� `c* `O `� `� `� `f" `I1 `�N `� `+0 `� `f" `  `� `� `� ` `� `�5 `n2 `�Q `& `8 `� `�" `jJ `�N `�6 `!Y `@ `�> `D5 `I< `J `�A `�K `�7 `= `w: `�< `/8 `q9 `Y= `�A `o8 `P4 `�  `� `j  `' `�E `� `"P `6 ` `m> `� `� `K `�. `1R `m `�* `� `\& `�" `� `r7 `�? `8A `�: `� `" `'> `�S `j. `�G `�- `�C `U `�) `�B `� `YQ `K `DF `� `� `5: `�% `� `4 `5 `M `} `I `` `, `E, `Y `]) ` ` `5 `�
 `.* `"! `� `�
 ` `
 `� `�) `' `o `�1 `=; `�( `�* `)) `�J `�' `% `�8 `�I `�/ `�; `yH `1 `�0 `;6 `X' `B `�7 `32 `sD `�6 `09 `{, `�V `�S `]@ `7+ `d0 `aB `�3 `�/ `BC `P! `E `�5 `�( `�F `K `yM `�C `�P `�B `�W `�, `�9 `�T `�D `� `� `R `@ `$ `p `� `f `^ `�< `7" `�Y `�  `!3 `�M `�& `�. `�! `�  `�? `~ `�@ `t `�' `U `�R `J/ `y `ZE `I
 `� `�) ` `U  `@ `� `-	 `	 `�	 `�	 `� `= `L `� `� `�
 `� `� `� `& `! `> `� `p `� `� `�  ` `� ` `� `� `� `v `K `j `� `: `% `� `� `w `: `� `� `p `1 ` `� `� `r  `� `# `Z `� `� `( ` `� `� `� `L `� `� `
 `� `  ` `0 `y `%  ` `/  `� `4N `a `\  `c  `�  `  `� `�  `�
 `XO `` `y ` `� `�+ `� `, ` `�
 `L ` `�9 `�# `Y `�4 `�; `fK `37 `�3 `$ `�# `2. `�2 `�% `�5 `f% `�: `S `z ` `	 `% `< `L `qI `� `� ` `*& ` ` `�G `�" `� `� `� `� `?? `�' `�$ `�& `v `0 `5% `~ `� `� `�$ `� `1
 `7 `B `�  `;  `G  `@ `N `�	 `Z `	  `  `  `  `�# `� `j  `*  `? `� `� `w `> `� `y
 `� `Y( `� `S ` `� ` `i `v `` `� `&D `Z `� `�% `: `�$ `� `� `� `� `w `U `: `�	 `H ` `� `� `� `r	 `_ ` `� `z `{ `� ` ` `� `* `� `�- `- `" `�# `� `� `� `5 `� `~! `�& `�+ `� `�, `� `8 ` `�  `z  `  `� `� `� `f `�	 `� `� `A  `� `� `	 `� `# `r `a `u `� `�  `�  `   `� `�  `b `� `K `� `u `- `N  ` `� `* `5  `�  `[	 `�  `D	 `�  `: `�> C? �? �? �@ �A �B C �C sH #I �I �I �J #K �K �L �O P CQ 'q Oq q �q �q r ;r gr �r �r �r s Gs ss �s �s �s 't Wt �t �t ku {v �v w x 